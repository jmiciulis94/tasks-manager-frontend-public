import { Action, GET_EMPLOYEE_REPORT, GET_MONTHLY_REPORT, GET_TASK_REPORT } from "../actions/action";
import {
    EmployeeReport,
    MonthlyReport,
    TaskReport
} from "../domain/report";
import { ReportState } from "../store/state";

import initialState from "./initialState";

export default function reportReducer (state: ReportState = initialState.report, action: Action<EmployeeReport> | Action<MonthlyReport> | Action<TaskReport>): ReportState {
    switch (action.type) {
        case GET_EMPLOYEE_REPORT: {
            const receivedEmployeeReport: EmployeeReport = action.data as EmployeeReport;
            return { ...state, employeeReport: receivedEmployeeReport };
        }
        case GET_MONTHLY_REPORT: {
            const receivedMonthlyReport: MonthlyReport = action.data as MonthlyReport;
            return { ...state, monthlyReport: receivedMonthlyReport };
        }
        case GET_TASK_REPORT: {
            const receivedTaskReport: TaskReport = action.data as TaskReport;
            return { ...state, taskReport: receivedTaskReport };
        }
        default:
            return state;
    }
}
