import { Action, CREATE_TASK, DELETE_TASK, GET_TASK, GET_TASK_LIST, UPDATE_TASK } from "../actions/action";
import { Task } from "../domain/project";
import { TaskState } from "../store/state";

import initialState from "./initialState";

export default function taskReducer (state: TaskState = initialState.task, action: Action<Task | Task[] | number>): TaskState {
    switch (action.type) {
        case GET_TASK: {
            const receivedEmployee: Task = action.data as Task;
            return { ...state, task: receivedEmployee };
        }
        case GET_TASK_LIST: {
            const receivedTaskList: Task[] = action.data as Task[];
            return { ...state, taskList: receivedTaskList };
        }
        case CREATE_TASK: {
            const createdTask: Task = action.data as Task;
            return { ...state, taskList: state.taskList.concat(createdTask) };
        }
        case UPDATE_TASK: {
            const updatedTask: Task = action.data as Task;
            return { ...state, taskList: state.taskList.map((t: Task) => t.id === updatedTask.id ? updatedTask : t) };
        }
        case DELETE_TASK: {
            const id: number = action.data as number;
            return { ...state, taskList: state.taskList.filter((t: Task) => t.id !== id) };
        }
        default:
            return state;
    }
}
