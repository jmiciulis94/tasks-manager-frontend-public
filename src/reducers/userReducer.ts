import { Action, GET_USER, UPDATE_USER } from "../actions/action";
import { User } from "../domain/user";
import { UserState } from "../store/state";

import initialState from "./initialState";

export default function userReducer (state: UserState = initialState.user, action: Action<User>): UserState {
    switch (action.type) {
        case GET_USER: {
            const receivedUser: User = action.data as User;
            return { ...state, user: receivedUser };
        }
        case UPDATE_USER: {
            const updatedUser: User = action.data as User;
            return { ...state, user: updatedUser };
        }
        default:
            return state;
    }
}
