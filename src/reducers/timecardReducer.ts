import { Action, CREATE_TIME_CARD, DELETE_TIME_CARD, GET_TIME_CARD, GET_TIME_CARD_LIST, UPDATE_TIME_CARD } from "../actions/action";
import { Timecard } from "../domain/timecard";
import { TimecardState } from "../store/state";

import initialState from "./initialState";

export default function timecardReducer (state: TimecardState = initialState.timecard, action: Action<Timecard | Timecard[] | number>): TimecardState {
    switch (action.type) {
        case GET_TIME_CARD: {
            const receivedEmployee: Timecard = action.data as Timecard;
            return { ...state, timecard: receivedEmployee };
        }
        case GET_TIME_CARD_LIST: {
            const receivedTimecardList: Timecard[] = action.data as Timecard[];
            return { ...state, timecardList: receivedTimecardList };
        }
        case CREATE_TIME_CARD: {
            const createdTimecard: Timecard = action.data as Timecard;
            return { ...state, timecardList: state.timecardList.concat(createdTimecard) };
        }
        case UPDATE_TIME_CARD: {
            const updatedTimecard: Timecard = action.data as Timecard;
            return { ...state, timecardList: state.timecardList.map((t: Timecard) => t.id === updatedTimecard.id ? updatedTimecard : t) };
        }
        case DELETE_TIME_CARD: {
            const id: number = action.data as number;
            return { ...state, timecardList: state.timecardList.filter((t: Timecard) => t.id !== id) };
        }
        default:
            return state;
    }
}
