import {
    Action, ADD_ERROR_MESSAGE,
    ADD_SUCCESS_MESSAGE,
    BEGIN_API_CALL, CLEAR_VALIDATION_ERROR, END_API_CALL,
    Message, REMOVE_ERROR_MESSAGE, REMOVE_SUCCESS_MESSAGE, SET_VALIDATION_ERROR
} from "../actions/action";
import { ApiState } from "../store/state";
import { ValidationError } from "../utils/apiUtils";

import initialState from "./initialState";

export default function apiReducer (state: ApiState = initialState.api, action: Action<Message | ValidationError | string | number>): ApiState {
    if (action.type === BEGIN_API_CALL) {
        return { ...state, numberOfApiGetInProgress: state.numberOfApiGetInProgress + 1 };
    } else if (action.type === END_API_CALL) {
        return { ...state, numberOfApiGetInProgress: state.numberOfApiGetInProgress - 1 };
    } else if (action.type === ADD_SUCCESS_MESSAGE) {
        const successMessage: Message = action.data as Message;
        return { ...state, successMessages: state.successMessages.concat(successMessage) };
    } else if (action.type === REMOVE_SUCCESS_MESSAGE) {
        const id: number = action.data as number;
        return { ...state, successMessages: state.successMessages.filter((m: Message) => m.id !== id) };
    } else if (action.type === ADD_ERROR_MESSAGE) {
        const errorMessage: Message = action.data as Message;
        return { ...state, errorMessages: state.errorMessages.concat(errorMessage) };
    } else if (action.type === REMOVE_ERROR_MESSAGE) {
        const id: number = action.data as number;
        return { ...state, errorMessages: state.errorMessages.filter((m: Message) => m.id !== id) };
    } else if (action.type === SET_VALIDATION_ERROR) {
        const validationError: ValidationError = action.data as ValidationError;
        return { ...state, validationError: validationError };
    } else if (action.type === CLEAR_VALIDATION_ERROR) {
        return { ...state, validationError: undefined };
    }

    return state;
}
