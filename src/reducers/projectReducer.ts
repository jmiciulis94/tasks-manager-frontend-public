import { Action, CREATE_PROJECT, DELETE_PROJECT, GET_PROJECT, GET_PROJECT_LIST, UPDATE_PROJECT } from "../actions/action";
import { Project } from "../domain/project";
import { ProjectState } from "../store/state";

import initialState from "./initialState";

export default function projectReducer (state: ProjectState = initialState.project, action: Action<Project | Project[] | number>): ProjectState {
    switch (action.type) {
        case GET_PROJECT: {
            const receivedEmployee: Project = action.data as Project;
            return { ...state, project: receivedEmployee };
        }
        case GET_PROJECT_LIST: {
            const receivedProjectList: Project[] = action.data as Project[];
            return { ...state, projectList: receivedProjectList };
        }
        case CREATE_PROJECT: {
            const createdProject: Project = action.data as Project;
            return { ...state, projectList: state.projectList.concat(createdProject) };
        }
        case UPDATE_PROJECT: {
            const updatedProject: Project = action.data as Project;
            return { ...state, projectList: state.projectList.map((p: Project) => p.id === updatedProject.id ? updatedProject : p) };
        }
        case DELETE_PROJECT: {
            const id: number = action.data as number;
            return { ...state, projectList: state.projectList.filter((p: Project) => p.id !== id) };
        }
        default:
            return state;
    }
}
