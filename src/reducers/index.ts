import { combineReducers } from "redux";

import api from "./apiReducer";
import employee from "./employeeReducer";
import period from "./periodReducer";
import project from "./projectReducer";
import report from "./reportReducer";
import speciality from "./specialityReducer";
import task from "./taskReducer";
import timecard from "./timecardReducer";
import user from "./userReducer";

const rootReducer = combineReducers({
    api,
    employee,
    period,
    project,
    report,
    speciality,
    task,
    timecard,
    user
});

export default rootReducer;
