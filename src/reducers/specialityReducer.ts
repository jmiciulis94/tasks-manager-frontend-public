import { Action, CREATE_SPECIALITY, DELETE_SPECIALITY, GET_SPECIALITY, GET_SPECIALITY_LIST, UPDATE_SPECIALITY } from "../actions/action";
import { Speciality } from "../domain/speciality";
import { SpecialityState } from "../store/state";

import initialState from "./initialState";

export default function specialityReducer (state: SpecialityState = initialState.speciality, action: Action<Speciality | Speciality[] | number>): SpecialityState {
    switch (action.type) {
        case GET_SPECIALITY: {
            const receivedEmployee: Speciality = action.data as Speciality;
            return { ...state, speciality: receivedEmployee };
        }
        case GET_SPECIALITY_LIST: {
            const receivedSpecialityList: Speciality[] = action.data as Speciality[];
            return { ...state, specialityList: receivedSpecialityList };
        }
        case CREATE_SPECIALITY: {
            const createdSpeciality: Speciality = action.data as Speciality;
            return { ...state, specialityList: state.specialityList.concat(createdSpeciality) };
        }
        case UPDATE_SPECIALITY: {
            const updatedSpeciality: Speciality = action.data as Speciality;
            return { ...state, specialityList: state.specialityList.map((s: Speciality) => s.id === updatedSpeciality.id ? updatedSpeciality : s) };
        }
        case DELETE_SPECIALITY: {
            const id: number = action.data as number;
            return { ...state, specialityList: state.specialityList.filter((s: Speciality) => s.id !== id) };
        }
        default:
            return state;
    }
}
