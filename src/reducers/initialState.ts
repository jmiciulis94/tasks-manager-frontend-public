import { Message } from "../actions/action";
import { Employee } from "../domain/employee";
import { Project, Task } from "../domain/project";
import { EmployeeReport, MonthlyReport, Period, TaskReport } from "../domain/report";
import { Speciality } from "../domain/speciality";
import { Timecard } from "../domain/timecard";
import { User } from "../domain/user";
import { ValidationError } from "../utils/apiUtils";

export type RootState = {
    api: {
        errorMessages: Message[];
        numberOfApiGetInProgress: number;
        successMessages: Message[];
        validationError?: ValidationError;
    };
    employee: {
        employee?: Employee;
        employeeList: Employee[];
    };
    period: {
        period?: Period;
        periodList: Period[];
    }
    project: {
        project?: Project;
        projectList: Project[];
    };
    report: {
        employeeReport?: EmployeeReport;
        monthlyReport?: MonthlyReport;
        taskReport?: TaskReport;
    };
    speciality: {
        speciality?: Speciality;
        specialityList: Speciality[];
    };
    task: {
        task?: Task;
        taskList: Task[];
    };
    timecard: {
        timecard?: Timecard;
        timecardList: Timecard[];
    };
    user: {
        user?: User;
    };
};

const initialState: RootState = {
    api: {
        errorMessages: [],
        numberOfApiGetInProgress: 0,
        successMessages: []
    },
    employee: {
        employeeList: []
    },
    period: {
        periodList: []
    },
    project: {
        projectList: []
    },
    report: {},
    speciality: {
        specialityList: []
    },
    task: {
        taskList: []
    },
    timecard: {
        timecardList: []
    },
    user: {}
};

export default initialState;
