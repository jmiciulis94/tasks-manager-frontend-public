import { Action, GET_PERIOD, GET_PERIOD_LIST } from "../actions/action";
import { Period } from "../domain/report";
import { PeriodState } from "../store/state";

import initialState from "./initialState";

export default function periodReducer (state: PeriodState = initialState.period, action: Action<Period | Period[]>): PeriodState {
    switch (action.type) {
        case GET_PERIOD: {
            const receivedPeriod: Period = action.data as Period;
            return { ...state, period: receivedPeriod };
        }
        case GET_PERIOD_LIST: {
            const receivedPeriodList: Period[] = action.data as Period[];
            return { ...state, periodList: receivedPeriodList };
        }
        default:
            return state;
    }
}
