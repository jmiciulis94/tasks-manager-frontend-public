import { Message } from "../actions/action";
import { Employee } from "../domain/employee";
import { Project, Task } from "../domain/project";
import { EmployeeReport, MonthlyReport, Period, TaskReport } from "../domain/report";
import { Speciality } from "../domain/speciality";
import { Timecard } from "../domain/timecard";
import { User } from "../domain/user";
import { ValidationError } from "../utils/apiUtils";

export type ApiState = {
    errorMessages: Message[];
    numberOfApiGetInProgress: number;
    successMessages: Message[];
    validationError?: ValidationError;
};

export type EmployeeState = {
    employee?: Employee;
    employeeList: Employee[];
};

export type PeriodState = {
    period?: Period;
    periodList: Period[];
};

export type ProjectState = {
    project?: Project;
    projectList: Project[];
};

export type ReportState = {
    employeeReport?: EmployeeReport;
    monthlyReport?: MonthlyReport;
    taskReport?: TaskReport;
};

export type SpecialityState = {
    speciality?: Speciality;
    specialityList: Speciality[];
};

export type TaskState = {
    task?: Task;
    taskList: Task[];
};

export type TimecardState = {
    timecard?: Timecard;
    timecardList: Timecard[];
};

export type UserState = {
    user?: User;
};
