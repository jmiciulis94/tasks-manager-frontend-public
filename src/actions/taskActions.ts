import { Dispatch } from "redux";

import { Task } from "../domain/project";

import {
    Action,
    CREATE_TASK,
    DELETE_TASK,
    deleteItem,
    GET_TASK,
    GET_TASK_LIST,
    getItem,
    getItemsList,
    saveItem,
    UPDATE_TASK
} from "./action";

export const getTaskSuccess = (task: Task): Action<Task> => {
    return {
        data: task,
        type: GET_TASK
    };
};

export const getTaskListSuccess = (taskList: Task[]): Action<Task[]> => {
    return {
        data: taskList,
        type: GET_TASK_LIST
    };
};

export const createTaskSuccess = (task: Task): Action<Task> => {
    return {
        data: task,
        type: CREATE_TASK
    };
};

export const updateTaskSuccess = (task: Task): Action<Task> => {
    return {
        data: task,
        type: UPDATE_TASK
    };
};

export const deleteTaskSuccess = (id: number): Action<number> => {
    return {
        data: id,
        type: DELETE_TASK
    };
};

export const getTask = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/task/" + id, getTaskSuccess);
};

export const getTaskList = (projectId: number): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/project/" + projectId + "/task", getTaskListSuccess);
};

export const saveTask = (task: Task): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/task", task, task.id ? updateTaskSuccess : createTaskSuccess, "Task successfully saved.");
};

export const deleteTask = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return deleteItem(process.env.API_URL + "/task/" + id, id, deleteTaskSuccess, "Task successfully deleted.");
};
