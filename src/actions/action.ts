import { Dispatch } from "redux";

import { ajaxDelete, ajaxGet, ajaxPost, ApiResponse } from "../utils/apiUtils";

import { addErrorMessage, addSuccessMessage, beginApiCall, endApiCall, setValidationError } from "./apiActions";

export const BEGIN_API_CALL = "BEGIN_API_CALL";
export const END_API_CALL = "END_API_CALL";
export const ADD_SUCCESS_MESSAGE = "ADD_SUCCESS_MESSAGE";
export const REMOVE_SUCCESS_MESSAGE = "REMOVE_SUCCESS_MESSAGE";
export const ADD_ERROR_MESSAGE = "ADD_ERROR_MESSAGE";
export const REMOVE_ERROR_MESSAGE = "REMOVE_ERROR_MESSAGE";
export const SET_VALIDATION_ERROR = "SET_VALIDATION_ERROR";
export const CLEAR_VALIDATION_ERROR = "CLEAR_VALIDATION_ERROR";

export const GET_EMPLOYEE = "GET_EMPLOYEE";
export const GET_EMPLOYEE_LIST = "GET_EMPLOYEE_LIST";
export const CREATE_EMPLOYEE = "CREATE_EMPLOYEE";
export const UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE";
export const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";

export const GET_PERIOD = "GET_PERIOD";
export const GET_PERIOD_LIST = "GET_PERIOD_LIST";

export const GET_PROJECT = "GET_PROJECT";
export const GET_PROJECT_LIST = "GET_PROJECT_LIST";
export const CREATE_PROJECT = "CREATE_PROJECT";
export const UPDATE_PROJECT = "UPDATE_PROJECT";
export const DELETE_PROJECT = "DELETE_PROJECT";

export const GET_TASK = "GET_TASK";
export const GET_TASK_LIST = "GET_TASK_LIST";
export const CREATE_TASK = "CREATE_TASK";
export const UPDATE_TASK = "UPDATE_TASK";
export const DELETE_TASK = "DELETE_TASK";

export const GET_SPECIALITY = "GET_SPECIALITY";
export const GET_SPECIALITY_LIST = "GET_SPECIALITY_LIST";
export const CREATE_SPECIALITY = "CREATE_SPECIALITY";
export const UPDATE_SPECIALITY = "UPDATE_SPECIALITY";
export const DELETE_SPECIALITY = "DELETE_SPECIALITY";

export const GET_TIME_CARD = "GET_TIME_CARD";
export const GET_TIME_CARD_LIST = "GET_TIME_CARD_LIST";
export const CREATE_TIME_CARD = "CREATE_TIME_CARD";
export const UPDATE_TIME_CARD = "UPDATE_TIME_CARD";
export const DELETE_TIME_CARD = "DELETE_TIME_CARD";

export const GET_USER = "GET_USER";
export const CREATE_USER = "SAVE_USER";
export const UPDATE_USER = "UPDATE_USER";

export const GET_EMPLOYEE_REPORT = "GET_EMPLOYEE_REPORT";
export const GET_TASK_REPORT = "GET_TASK_REPORT";
export const GET_MONTHLY_REPORT = "GET_MONTHLY_REPORT";

export type Action<T> = {
    data?: T;
    type: string;
};

export type Message = {
    id: number;
    message: string;
};

const createErrorMessage = <T>(response: ApiResponse<T>): Message => {
    const id = new Date().getTime();
    return {
        id: id,
        message: response.error ? response.error.header : "Server error. Please try again later."
    };
};

const createSuccessMessage = (message: string): Message => {
    const id = new Date().getTime();
    return {
        id: id,
        message: message
    };
};

export const getItem = <T>(url: string, successFunction: (item: T) => Action<T>) => {
    return async function (dispatch: Dispatch): Promise<void> {
        dispatch(beginApiCall());
        const response: ApiResponse<T> = await ajaxGet<T>(url);
        dispatch(endApiCall());

        if (response.status === 200 && response.data) {
            dispatch(successFunction(response.data));
        } else {
            dispatch(addErrorMessage(createErrorMessage(response)));
        }
    };
};

export const getItemsList = <T>(url: string, successFunction: (items: T[]) => Action<T[]>) => {
    return async function (dispatch: Dispatch): Promise<void> {
        dispatch(beginApiCall());
        const response: ApiResponse<T[]> = await ajaxGet<T[]>(url);
        dispatch(endApiCall());

        if (response.status === 200 && response.data) {
            dispatch(successFunction(response.data));
        } else {
            dispatch(addErrorMessage(createErrorMessage(response)));
        }
    };
};

export const saveItem = <T>(url: string, item: T, successFunction: (item: T) => Action<T>, successMessage: string) => {
    return async function (dispatch: Dispatch): Promise<boolean> {
        dispatch(beginApiCall());
        const response: ApiResponse<T> = await ajaxPost<T>(url, item);
        dispatch(endApiCall());

        if (response.status === 200 && response.data) {
            dispatch(successFunction(response.data));
            dispatch(addSuccessMessage(createSuccessMessage(successMessage)));
            return true;
        } else if (response.status === 400 && response.validationError) {
            dispatch(setValidationError(response.validationError));
            return false;
        } else {
            dispatch(addErrorMessage(createErrorMessage(response)));
            return false;
        }
    };
};

export const deleteItem = (url: string, id: number, successFunction: (id: number) => Action<number>, successMessage: string) => {
    return async function (dispatch: Dispatch): Promise<void> {
        dispatch(beginApiCall());
        const response: ApiResponse<void> = await ajaxDelete(url);
        dispatch(endApiCall());

        if (response.status === 204) {
            dispatch(successFunction(id));
            dispatch(addSuccessMessage(createSuccessMessage(successMessage)));
        } else {
            dispatch(addErrorMessage(createErrorMessage(response)));
        }
    };
};
