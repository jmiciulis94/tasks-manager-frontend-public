import { Dispatch } from "redux";

import { User } from "../domain/user";

import { Action, CREATE_USER, GET_USER, getItem, saveItem, UPDATE_USER } from "./action";

export const getUserSuccess = (user: User): Action<User> => {
    return {
        data: user,
        type: GET_USER
    };
};

export const createUserSuccess = (user: User): Action<User> => {
    return {
        data: user,
        type: CREATE_USER
    };
};

export const updateUserSuccess = (user: User): Action<User> => {
    return {
        data: user,
        type: UPDATE_USER
    };
};

export const getUser = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/user/" + id, getUserSuccess);
};

export const saveUser = (user: User): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/user", user, user.id ? updateUserSuccess : createUserSuccess, "User successfully saved.");
};
