import { Dispatch } from "redux";

import { Speciality } from "../domain/speciality";

import {
    Action,
    CREATE_SPECIALITY,
    DELETE_SPECIALITY,
    deleteItem,
    GET_SPECIALITY,
    GET_SPECIALITY_LIST,
    getItem,
    getItemsList,
    saveItem,
    UPDATE_SPECIALITY
} from "./action";

export const getSpecialitySuccess = (speciality: Speciality): Action<Speciality> => {
    return {
        data: speciality,
        type: GET_SPECIALITY
    };
};

export const getSpecialityListSuccess = (specialityList: Speciality[]): Action<Speciality[]> => {
    return {
        data: specialityList,
        type: GET_SPECIALITY_LIST
    };
};

export const createSpecialitySuccess = (speciality: Speciality): Action<Speciality> => {
    return {
        data: speciality,
        type: CREATE_SPECIALITY
    };
};

export const updateSpecialitySuccess = (speciality: Speciality): Action<Speciality> => {
    return {
        data: speciality,
        type: UPDATE_SPECIALITY
    };
};

export const deleteSpecialitySuccess = (id: number): Action<number> => {
    return {
        data: id,
        type: DELETE_SPECIALITY
    };
};

export const getSpeciality = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/speciality/" + id, getSpecialitySuccess);
};

export const getSpecialityList = (): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/speciality", getSpecialityListSuccess);
};

export const saveSpeciality = (speciality: Speciality): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/speciality", speciality, speciality.id ? updateSpecialitySuccess : createSpecialitySuccess, "Speciality successfully saved.");
};

export const deleteSpeciality = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return deleteItem(process.env.API_URL + "/speciality/" + id, id, deleteSpecialitySuccess, "Speciality successfully deleted.");
};
