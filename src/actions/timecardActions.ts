import { Dispatch } from "redux";

import { Timecard } from "../domain/timecard";

import {
    Action,
    CREATE_TIME_CARD,
    DELETE_TIME_CARD,
    deleteItem,
    GET_TIME_CARD,
    GET_TIME_CARD_LIST,
    getItem,
    getItemsList,
    saveItem,
    UPDATE_TIME_CARD
} from "./action";

export const getTimecardSuccess = (timecard: Timecard): Action<Timecard> => {
    return {
        data: timecard,
        type: GET_TIME_CARD
    };
};

export const getTimecardListSuccess = (timecardList: Timecard[]): Action<Timecard[]> => {
    return {
        data: timecardList,
        type: GET_TIME_CARD_LIST
    };
};

export const createTimecardSuccess = (timecard: Timecard): Action<Timecard> => {
    return {
        data: timecard,
        type: CREATE_TIME_CARD
    };
};

export const updateTimecardSuccess = (timecard: Timecard): Action<Timecard> => {
    return {
        data: timecard,
        type: UPDATE_TIME_CARD
    };
};

export const deleteTimecardSuccess = (id: number): Action<number> => {
    return {
        data: id,
        type: DELETE_TIME_CARD
    };
};

export const getTimecard = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/timecard/" + id, getTimecardSuccess);
};

export const getTimecardList = (employeeId: number): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/timecard?employeeId=" + employeeId, getTimecardListSuccess);
};

export const saveTimecard = (timecard: Timecard): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/timecard", timecard, timecard.id ? updateTimecardSuccess : createTimecardSuccess, "Timecard successfully saved.");
};

export const deleteTimecard = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return deleteItem(process.env.API_URL + "/timecard/" + id, id, deleteTimecardSuccess, "Timecard successfully deleted.");
};
