import { Dispatch } from "redux";

import { Employee } from "../domain/employee";

import {
    Action,
    CREATE_EMPLOYEE,
    DELETE_EMPLOYEE,
    deleteItem,
    GET_EMPLOYEE,
    GET_EMPLOYEE_LIST,
    getItem,
    getItemsList,
    saveItem,
    UPDATE_EMPLOYEE
} from "./action";

export const getEmployeeSuccess = (employee: Employee): Action<Employee> => {
    return {
        data: employee,
        type: GET_EMPLOYEE
    };
};

export const getEmployeeListSuccess = (employeeList: Employee[]): Action<Employee[]> => {
    return {
        data: employeeList,
        type: GET_EMPLOYEE_LIST
    };
};

export const createEmployeeSuccess = (employee: Employee): Action<Employee> => {
    return {
        data: employee,
        type: CREATE_EMPLOYEE
    };
};

export const updateEmployeeSuccess = (employee: Employee): Action<Employee> => {
    return {
        data: employee,
        type: UPDATE_EMPLOYEE
    };
};

export const deleteEmployeeSuccess = (id: number): Action<number> => {
    return {
        data: id,
        type: DELETE_EMPLOYEE
    };
};

export const getEmployee = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/employee/" + id, getEmployeeSuccess);
};

export const getEmployeeList = (): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/employee", getEmployeeListSuccess);
};

export const saveEmployee = (employee: Employee): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/employee", employee, employee.id ? updateEmployeeSuccess : createEmployeeSuccess, "Employee successfully saved.");
};

export const deleteEmployee = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return deleteItem(process.env.API_URL + "/employee/" + id, id, deleteEmployeeSuccess, "Employee successfully deleted.");
};
