import { Dispatch } from "redux";

import {
    EmployeeReport,
    MonthlyReport,
    TaskReport
} from "../domain/report";

import { Action, GET_EMPLOYEE_REPORT, GET_MONTHLY_REPORT, GET_TASK_REPORT, getItem } from "./action";

export const getEmployeeReportSuccess = (employeeReport: EmployeeReport): Action<EmployeeReport> => {
    return {
        data: employeeReport,
        type: GET_EMPLOYEE_REPORT
    };
};

export const getMonthlyReportSuccess = (monthlyReport: MonthlyReport): Action<MonthlyReport> => {
    return {
        data: monthlyReport,
        type: GET_MONTHLY_REPORT
    };
};

export const getTaskReportSuccess = (taskReport: TaskReport): Action<TaskReport> => {
    return {
        data: taskReport,
        type: GET_TASK_REPORT
    };
};

export const getEmployeeReport = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/report/employee/" + id, getEmployeeReportSuccess);
};

export const getMonthlyReport = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/report/period/" + id, getMonthlyReportSuccess);
};

export const getTaskReport = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/report/task/" + id, getTaskReportSuccess);
};
