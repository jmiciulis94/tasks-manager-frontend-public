import { Dispatch } from "redux";

import { Project } from "../domain/project";

import {
    Action,
    CREATE_PROJECT,
    DELETE_PROJECT,
    deleteItem,
    GET_PROJECT,
    GET_PROJECT_LIST,
    getItem,
    getItemsList,
    saveItem,
    UPDATE_PROJECT
} from "./action";

export const getProjectSuccess = (project: Project): Action<Project> => {
    return {
        data: project,
        type: GET_PROJECT
    };
};

export const getProjectListSuccess = (projectList: Project[]): Action<Project[]> => {
    return {
        data: projectList,
        type: GET_PROJECT_LIST
    };
};

export const createProjectSuccess = (project: Project): Action<Project> => {
    return {
        data: project,
        type: CREATE_PROJECT
    };
};

export const updateProjectSuccess = (project: Project): Action<Project> => {
    return {
        data: project,
        type: UPDATE_PROJECT
    };
};

export const deleteProjectSuccess = (id: number): Action<number> => {
    return {
        data: id,
        type: DELETE_PROJECT
    };
};

export const getProject = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/project/" + id, getProjectSuccess);
};

export const getProjectList = (): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/project", getProjectListSuccess);
};

export const saveProject = (project: Project): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/project", project, project.id ? updateProjectSuccess : createProjectSuccess, "Project successfully saved.");
};

export const deleteProject = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return deleteItem(process.env.API_URL + "/project/" + id, id, deleteProjectSuccess, "Project successfully deleted.");
};
