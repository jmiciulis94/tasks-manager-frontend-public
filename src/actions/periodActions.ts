import { Dispatch } from "redux";

import { Period } from "../domain/report";

import { Action, GET_PERIOD, GET_PERIOD_LIST, getItem, getItemsList } from "./action";

export const getPeriodSuccess = (period: Period): Action<Period> => {
    return {
        data: period,
        type: GET_PERIOD
    };
};

export const getPeriodListSuccess = (periodList: Period[]): Action<Period[]> => {
    return {
        data: periodList,
        type: GET_PERIOD_LIST
    };
};

export const getPeriod = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/period/" + id, getPeriodSuccess);
};

export const getPeriodList = (): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/period", getPeriodListSuccess);
};
