import { ValidationError } from "../utils/apiUtils";

import {
    Action,
    ADD_ERROR_MESSAGE,
    ADD_SUCCESS_MESSAGE,
    BEGIN_API_CALL,
    CLEAR_VALIDATION_ERROR,
    END_API_CALL,
    Message,
    REMOVE_ERROR_MESSAGE,
    REMOVE_SUCCESS_MESSAGE,
    SET_VALIDATION_ERROR
} from "./action";

export const beginApiCall = (): Action<void> => {
    return {
        type: BEGIN_API_CALL
    };
};

export const endApiCall = (): Action<void> => {
    return {
        type: END_API_CALL
    };
};

export const addSuccessMessage = (message: Message): Action<Message> => {
    return {
        data: message,
        type: ADD_SUCCESS_MESSAGE
    };
};

export const removeSuccessMessage = (id: number): Action<number> => {
    return {
        data: id,
        type: REMOVE_SUCCESS_MESSAGE
    };
};

export const addErrorMessage = (message: Message): Action<Message> => {
    return {
        data: message,
        type: ADD_ERROR_MESSAGE
    };
};

export const removeErrorMessage = (id: number): Action<number> => {
    return {
        data: id,
        type: REMOVE_ERROR_MESSAGE
    };
};

export const setValidationError = (validationError: ValidationError): Action<ValidationError> => {
    return {
        data: validationError,
        type: SET_VALIDATION_ERROR
    };
};

export const clearValidationError = (): Action<void> => {
    return {
        type: CLEAR_VALIDATION_ERROR
    };
};
