import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { Template } from "smart-tech-solutions-frontend-components";
import "smart-tech-solutions-frontend-components/lib/Styles.css";

import AppMenu from "./AppMenu";
import EmployeeListPage from "./containers/EmployeeListPage";
import EmployeePage from "./containers/EmployeePage";
import EmployeeReportPage from "./containers/EmployeeReportPage";
import HomePage from "./containers/HomePage";
import MonthlyReportPage from "./containers/MonthlyReportPage";
import ProjectListPage from "./containers/ProjectListPage";
import ProjectPage from "./containers/ProjectPage";
import SettingsPage from "./containers/SettingsPage";
import TaskReportPage from "./containers/TaskReportPage";
import TimecardListPage from "./containers/TimecardListPage";

const menu = <AppMenu />;

const App = (): JSX.Element => (
    <>
        <Template currentLanguageCode="us" email="jonmic@gmail.com" fullName="Jonas Mičiulis" menu={menu} profileImagePath="/static/images/users/5.jpg">
            <Switch>
                <Route component={HomePage} exact={true} path="/" />
                <Route component={EmployeePage} path="/employee/:id" />
                <Route component={EmployeePage} path="/employee" />
                <Route component={EmployeeListPage} path="/employee-list" />
                <Route component={EmployeeReportPage} path="/employee-report" />
                <Route component={MonthlyReportPage} path="/monthly-report" />
                <Route component={ProjectPage} path="/project/:id" />
                <Route component={ProjectListPage} path="/project-list" />
                <Route component={SettingsPage} path="/settings" />
                <Route component={TaskReportPage} path="/task-report" />
                <Route component={TimecardListPage} path="/timecard-list" />
                <Redirect to="/" />
            </Switch>
        </Template>
    </>
);

export default App;
