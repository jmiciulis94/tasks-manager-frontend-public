export type ApiResponse<T> = {
    data?: T;
    error?: SystemError;
    status: number;
    validationError?: ValidationError;
};

export type ValidationError = {
    header: string;
    items: string[];
};

export type SystemError = {
    header: string;
};

const ajax = async <T>(url: string, options: RequestInit): Promise<ApiResponse<T>> => {
    const response: Response = await fetch(url, options);
    if (response.status === 204) {
        return {
            status: response.status
        };
    } else if (response.status === 200) {
        const data: T = await response.json() as T;
        return {
            data: data,
            status: response.status
        };
    } else if (response.status === 400) {
        const validationError: ValidationError = await response.json() as ValidationError;
        return {
            status: response.status,
            validationError: validationError
        };
    } else {
        const error: SystemError = await response.json() as SystemError;
        return {
            error: error,
            status: response.status
        };
    }
};

export const ajaxGet = <T>(url: string): Promise<ApiResponse<T>> => {
    const options: RequestInit = {
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        method: "GET"
    };
    return ajax(url, options);
};

export const ajaxPost = <T>(url: string, data: T): Promise<ApiResponse<T>> => {
    const options: RequestInit = {
        body: JSON.stringify(data),
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        method: "POST"
    };
    return ajax(url, options);
};

export const ajaxDelete = (url: string): Promise<ApiResponse<void>> => {
    const options: RequestInit = {
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        method: "DELETE"
    };
    return ajax(url, options);
};
