import { FormOption } from "smart-tech-solutions-frontend-components";

export const getYesNoOptions = (): FormOption[] => {
    return [
        {
            label: "Yes",
            value: "true"
        },
        {
            label: "No",
            value: "false"
        }
    ];
};
