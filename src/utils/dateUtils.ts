export const currentDate = (): string => {
    const now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDay() + 1).toISOString().substring(0, 10);
};

export const daysInMonth = (date: string): number => {
    return new Date(parseInt(date.substring(0, 4)), parseInt(date.substring(5, 7)), 0).getDate();
};

export const dayToDate = (day: number, date: string): string => {
    return new Date(parseInt(date.substring(0, 4)), parseInt(date.substring(5, 7)) - 1, day + 1).toISOString().substring(0, 10);
};
