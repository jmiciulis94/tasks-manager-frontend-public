import { FormOption } from "smart-tech-solutions-frontend-components";

import { Speciality } from "./speciality";

export const FIRST_DAY_OF_THE_WEEK_MONDAY = "Monday";
export const FIRST_DAY_OF_THE_WEEK_SUNDAY = "Sunday";
export const PERIOD_TYPE_MONTHLY = "Monthly";
export const PERIOD_TYPE_WEEKLY = "Weekly";

export type User = {
    currency: string;
    dateFormat: string;
    email?: string;
    firstDayOfTheWeek: string;
    firstName?: string;
    id?: string;
    lastName?: string;
    numberFormat: string;
    password?: string;
    periodType: string;
    specialities: Speciality[];
    username?: string;
};

export const getNumberFormatOptions = (): FormOption[] => {
    return [
        {
            label: "23,456.78",
            value: "#,###.##"
        },
        {
            label: "23.456,78",
            value: "#.###,##"
        },
        {
            label: "23456.78",
            value: "####.##"
        },
        {
            label: "23456,78",
            value: "####,##"
        }
    ];
};

export const getCurrencyOptions = (): FormOption[] => {

    const currencyList: string[] = [ "€ (EUR)", "$ (USD)", "£ (GBP)", "₡ (CRC)", "₪ (ILS)", "₹ (INR)", "¥ (JPY)", "₩ (KRW)", "₦ (NGN)", "₱ (PHP)", "zł (PLN)", "₲ (PYG)", "฿ (THB)", "₴ (UAH)", "₫ (VND)" ];

    return currencyList.map((currency: string) => {
        return {
            label: currency,
            value: currency.split(" ", 1).toLocaleString()
        };
    });
};

export const getDateFormatOptions = (): FormOption[] => {
    return [
        {
            label: "2020-06-23",
            value: "yyyy-mm-dd"
        },
        {
            label: "2020/06/23",
            value: "yyyy/mm/dd"
        },
        {
            label: "06/23/2020",
            value: "mm/dd/yyyy"
        },
        {
            label: "23/06/2020",
            value: "dd/mm/yyyy"
        },
        {
            label: "June 23 2020",
            value: "Month dd yyyy"
        },
        {
            label: "June 23, 2020",
            value: "Month dd, yyyy"
        }
    ];
};

export const getFirstDayOfTheWeekOptions = (): FormOption[] => {
    return [
        {
            label: "Monday",
            value: FIRST_DAY_OF_THE_WEEK_MONDAY
        },
        {
            label: "Sunday",
            value: FIRST_DAY_OF_THE_WEEK_SUNDAY
        }
    ];
};

export const getPeriodType = (): FormOption[] => {
    return [
        {
            label: "Monthly",
            value: PERIOD_TYPE_MONTHLY
        },
        {
            label: "Weekly",
            value: PERIOD_TYPE_WEEKLY
        }
    ];
};
