import moment, { Moment } from "moment";
import { FormOption } from "smart-tech-solutions-frontend-components";

import { MONTHS } from "./report";

export type PeriodBudget = {
    amount: number;
    id?: number;
    month?: number;
    week?: number;
    year: number;
}

export type Task = {
    estimatedCost?: number;
    estimatedEndDate?: string;
    id?: number;
    isPaidByFact: boolean;
    periodBudgets: PeriodBudget[];
    startDate?: string;
    title: string;
};

export type Project = {
    description?: string;
    estimatedCost?: number;
    estimatedEndDate?: string;
    id?: number;
    startDate?: string;
    tasks: Task[];
    title: string;
};

export const formattedPeriod = (periodBudget: PeriodBudget): string => {
    if (periodBudget.month) {
        return "" + MONTHS.get(periodBudget.month) + ", " + periodBudget.year;
    }

    return "";
};

export const getPeriodsForms = (startDate: string, endDate: string, existingPeriods: PeriodBudget[]): PeriodBudget[] => {
    const periods: PeriodBudget[] = [];
    const startDateMoment: Moment = moment(startDate);
    const endDateMoment: Moment = moment(endDate);

    if (startDateMoment.isAfter(endDateMoment)) {
        return [];
    }

    const firstDay: Moment = moment(startDate).startOf("month");
    const lastDay: Moment = moment(endDate).endOf("month");

    while (firstDay.isBefore(lastDay)) {
        const year = firstDay.year();
        const month = firstDay.month() + 1;
        const existingPeriod: PeriodBudget | undefined = existingPeriods.find((p: PeriodBudget) => p.year === year && p.month === month);

        if (existingPeriod) {
            periods.push(existingPeriod);
        } else {
            const periodBudget: PeriodBudget = {
                amount: 0,
                month: month,
                year: year
            };
            periods.push(periodBudget);
        }

        firstDay.add(1, "month");
    }

    return periods;
};

export const getProjectOptions = (projectList: Project[]): FormOption[] => {
    return projectList.map((project: Project) => {
        return {
            label: project.title,
            value: project.id || ""
        };
    });
};

export const getTaskOptions = (taskList: Task[]): FormOption[] => {
    return taskList.map((task: Task) => {
        return {
            label: task.title,
            value: task.id || ""
        };
    });
};
