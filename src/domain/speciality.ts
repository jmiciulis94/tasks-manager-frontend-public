import { FormOption } from "smart-tech-solutions-frontend-components";

export type Speciality = {
    id?: number;
    title: string;
};

export const getSpecialityOptions = (specialityList: Speciality[]): FormOption[] => {
    return specialityList.map((speciality: Speciality) => {
        return {
            label: speciality.title,
            value: speciality.id || ""
        };
    });
};
