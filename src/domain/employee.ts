import { FormOption } from "smart-tech-solutions-frontend-components";

import { Speciality } from "./speciality";

export const TYPE_OF_PAY_NONE = "NONE";
export const TYPE_OF_PAY_HOURLY = "HOURLY";
export const TYPE_OF_PAY_RATE = "RATE";

export type Employee = {
    age?: number;
    dateOfBirth?: string;
    email?: string;
    firstName: string;
    id?: number;
    lastName?: string;
    payPerHour?: number;
    payRate?: string;
    phone?: string;
    rate?: number;
    specialityIds?: number[];
    specialityTitles?: string[];
    startDate?: string;
    typeOfPay: string;
};

export const fullName = (firstName: string, lastName?: string | undefined): string => {
    return lastName ? firstName + " " + lastName : firstName;
};

export const formattedSpecialities = (specialityIds: number[] | undefined, allSpecialities: Speciality[]): string => {
    let result = "";
    if (specialityIds) {
        specialityIds.forEach((specialityId: number, index: number) => {
            if (index !== 0) {
                result = result + ", ";
            }

            const speciality: Speciality | undefined = allSpecialities.find((s: Speciality) => specialityId == s.id);
            if (speciality) {
                result = result + speciality.title;
            }
        });
    }

    return result;
};

export const formattedTypeOfPay = (type: string): string => {
    if (type === TYPE_OF_PAY_NONE) {
        return "None";
    } else if (type === TYPE_OF_PAY_HOURLY) {
        return "Hourly";
    } else if (type === TYPE_OF_PAY_RATE) {
        return "Rate";
    } else {
        return "";
    }
};

export const formattedPayPerHour = (payPerHour?: number, currency?: string): string => {
    return payPerHour && currency ? payPerHour.toString() + currency : payPerHour ? payPerHour.toString() : "";
};

export const getEmployeeOptions = (employeeList: Employee[]): FormOption[] => {
    return employeeList.map((employee: Employee) => {
        return {
            label: fullName(employee.firstName, employee.lastName),
            value: employee.id || ""
        };
    });
};

export const getTypeOfPayOptions = (): FormOption[] => {
    return [
        {
            label: "None",
            value: TYPE_OF_PAY_NONE
        },
        {
            label: "Hourly",
            value: TYPE_OF_PAY_HOURLY
        },
        {
            label: "Rate",
            value: TYPE_OF_PAY_RATE
        }
    ];
};
