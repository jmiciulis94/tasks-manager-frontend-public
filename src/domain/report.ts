import { FormOption } from "smart-tech-solutions-frontend-components";

import { Employee, TYPE_OF_PAY_HOURLY, TYPE_OF_PAY_NONE, TYPE_OF_PAY_RATE } from "./employee";

export const MONTHS = new Map();
MONTHS.set(1, "January");
MONTHS.set(2, "February");
MONTHS.set(3, "March");
MONTHS.set(4, "April");
MONTHS.set(5, "May");
MONTHS.set(6, "June");
MONTHS.set(7, "July");
MONTHS.set(8, "August");
MONTHS.set(9, "September");
MONTHS.set(10, "October");
MONTHS.set(11, "November");
MONTHS.set(12, "December");

export type Period = {
    id?: number;
    month?: number;
    week?: number;
    year: number;
};

export type PeriodTimesheet = {
    period: Period;
    timesheets: Timesheet[];
};

export type EmployeeTimesheet = {
    employee: Employee;
    timesheets: Timesheet[];
};

export type Timesheet = {
    amount: number;
    employeeId: number;
    id: number;
    minutes: number;
    payPerHour?: number;
    period: Period;
    rate?: number;
    taskId: number;
    typeOfPay: string;
};

export type EmployeeHeader = {
    employeeId: number;
    fullName: string;
    totalAmount: number;
    totalMinutes: number;
};

export type ProjectHeader = {
    numberOfTasks: number;
    projectId: number;
    title: string;
};

export type TaskHeader = {
    taskId: number;
    title: string;
    totalAmount: number;
    totalMinutes: number;
};

export type Totals = {
    amount: number;
    minutes: number;
};

export type EmployeeReport = {
    entries: PeriodTimesheet[];
    projects: ProjectHeader[];
    tasks: TaskHeader[];
    totals: Totals;
};

export type MonthlyReport = {
    entries: EmployeeTimesheet[];
    tasks: TaskHeader[];
    totals: Totals;
};

export type TaskReport = {
    employees: EmployeeHeader[];
    entries: PeriodTimesheet[];
    totals: Totals;
};

export const formattedPayRate = (timesheet: Timesheet, type: string): number | undefined | string => {
    if (type === TYPE_OF_PAY_NONE) {
        return "Unbilled";
    } else if (type === TYPE_OF_PAY_HOURLY) {
        return timesheet.payPerHour + "€/h";
    } else if (type === TYPE_OF_PAY_RATE) {
        return timesheet.rate;
    } else {
        return "";
    }
};

export const formattedWithCurrency = (num: number): string => {
    return num + "€";
};

export const formattedWithMinutes = (num: number): string => {
    return num + "min";
};

export const formattedDate = (year: number, month: number | undefined): string => {
    const monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    if (month) {
        const newMonth = monthNames[month - 1];
        return newMonth + ", " + year;
    } else {
        return year.toLocaleString();
    }
};

export const getPeriodOptions = (periodList: Period[]): FormOption[] => {
    return periodList.map((period: Period) => {
        return {
            label: formattedDate(period.year, period.month),
            value: period.id || ""
        };
    });
};
