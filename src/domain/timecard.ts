export type Timecard = {
    date: string;
    description?: string;
    employeeFullName?: string;
    employeeId: number;
    id?: number;
    minutes: number;
    projectId: number;
    projectTitle?: string;
    taskId: number;
    taskTitle?: string;
};
