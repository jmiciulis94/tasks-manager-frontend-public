import React from "react";
import { Button, FormSection, FormValue, FormWrapper, Table } from "smart-tech-solutions-frontend-components";

import { Speciality } from "../../domain/speciality";
import { User } from "../../domain/user";

export type ViewSettingsProps = {
    onEdit: () => void;
    user: User;
};

const ViewSettings = ({ user, onEdit }: ViewSettingsProps): JSX.Element => (
    <FormWrapper theme="view">
        <FormSection title="Number settings">
            <FormValue label="Number format" value={user.numberFormat} />
            <FormValue label="Currency" value={user.currency} />
        </FormSection>
        <FormSection title="Date settings">
            <FormValue label="Date format" value={user.dateFormat} />
            <FormValue label="First day of the week" value={user.firstDayOfTheWeek} />
            <FormValue label="Period" value={user.periodType} />
        </FormSection>
        <FormSection title="Specialities">
            <Table headers={[ "Title" ]}>
                {user.specialities.map((speciality: Speciality) =>
                    <tr key={speciality.id}>
                        <td>{speciality.title}</td>
                    </tr>
                )}
            </Table>
        </FormSection>
        <Button text="Edit settings" theme="success" onClick={onEdit} />
    </FormWrapper>
);

export default ViewSettings;
