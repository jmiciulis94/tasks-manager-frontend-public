import { FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router";
import { BreadcrumbEntry, Page } from "smart-tech-solutions-frontend-components";

import * as apiActions from "../../actions/apiActions";
import * as specialityActions from "../../actions/specialityActions";
import * as userActions from "../../actions/userActions";
import { User } from "../../domain/user";
import { RootState } from "../../reducers/initialState";

import EditSettings from "./EditSettings";
import ViewSettings from "./ViewSettings";

const mapStateToProps = (state: RootState) => {
    return {
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        specialityList: state.speciality.specialityList,
        successMessages: state.api.successMessages,
        user: state.user.user,
        validationError: state.api.validationError
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    getSpecialityList: specialityActions.getSpecialityList,
    getUser: userActions.getUser,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveUser: userActions.saveUser
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type AllSettingsPageProps = RouteComponentProps & PropsFromRedux;

const emptyUser: User = {
    currency: "$",
    dateFormat: "yyyy-mm-dd",
    firstDayOfTheWeek: "Monday",
    numberFormat: "#,###.##",
    periodType: "Monthly",
    specialities: []
};

const SettingsPage = (props: AllSettingsPageProps): JSX.Element => {
    const [ user, setUser ] = useState<User>(emptyUser);
    const [ editMode, setEditMode ] = useState<boolean>(false);

    useEffect(() => {
        props.getSpecialityList();
    }, []);

    useEffect(() => {
        if (props.user) {
            setUser(props.user);
        }
    }, [ props.user ]);

    useEffect(() => {
        const params: URLSearchParams = new URLSearchParams(props.location.search);
        const mode: string | null = params.get("mode");
        props.clearValidationError();
        props.getUser(1); // toDo: after user implemented -> change
        mode && mode === "edit" ? setEditMode(true) : setEditMode(false);
    }, [ props.match.params ]);

    const handleSubmit = async (user: User, actions: FormikHelpers<User>): Promise<void> => {
        const success: boolean = await props.saveUser(user);
        if (success) {
            actions.resetForm();
            props.clearValidationError();
            props.history.goBack();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleBack = (): void => {
        props.history.goBack();
    };

    const handleEdit = (): void => {
        props.history.push("/settings?mode=edit");
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Settings", url: "/settings" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title={"Settings"}>
            {editMode ? <EditSettings user={user} validationError={props.validationError} onCancel={handleBack} onSubmit={handleSubmit} /> : <ViewSettings user={user} onEdit={handleEdit} />}
        </Page>
    );
};

export default connector(SettingsPage);
