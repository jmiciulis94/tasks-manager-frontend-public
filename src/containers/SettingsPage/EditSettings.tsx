import { FieldArray, FieldArrayRenderProps, Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { Button, FormActions, FormSection, FormSelect, FormValidationErrors, FormWrapper, Table } from "smart-tech-solutions-frontend-components";
import { array, object, SchemaOf, string } from "yup";

import { Speciality } from "../../domain/speciality";
import { getCurrencyOptions, getDateFormatOptions, getFirstDayOfTheWeekOptions, getNumberFormatOptions, getPeriodType, User } from "../../domain/user";
import { ValidationError } from "../../utils/apiUtils";

import SpecialityFields from "./SpecialityFields";

export type EditEmployeeProps = {
    onCancel: () => void;
    onSubmit: (values: User, actions: FormikHelpers<User>) => void;
    user: User;
    validationError?: ValidationError;
};

const settingsValidationSchema: SchemaOf<User> = object({
    currency: string().required("This field is required"),
    dateFormat: string().required("This field is required"),
    firstDayOfTheWeek: string().required("This field is required"),
    numberFormat: string().required("This field is required"),
    periodType: string().required("This field is required"),
    specialities: array().of(
        object<Speciality>().shape({
            title: string().required("This field is required").max(50, "Can not be longer than 50 symbols")
        }).required()
    )
});

const EditSettings = ({ user, onCancel, onSubmit, validationError }: EditEmployeeProps): JSX.Element => {

    const addSpeciality = (): Speciality => {
        return {
            id: 0,
            title: ""
        };
    };

    return (
        <>
            <FormValidationErrors validationError={validationError} />
            <Formik enableReinitialize={true} initialValues={user} validateOnBlur={false} validateOnChange={false} validationSchema={settingsValidationSchema} onSubmit={onSubmit}>
                {({ values, errors, isSubmitting, setFieldValue, submitCount }: FormikProps<User>) => (
                    <FormWrapper>
                        <FormSection title="Number settings">
                            <FormSelect invalid={!!errors.numberFormat} label="Number format:" name="numberFormat" options={getNumberFormatOptions()} validated={submitCount > 0} value={values.numberFormat} onChange={setFieldValue} />
                            <FormSelect invalid={!!errors.currency} label="Currency:" name="currency" options={getCurrencyOptions()} validated={submitCount > 0} value={values.currency} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Date settings">
                            <FormSelect invalid={!!errors.dateFormat} label="Date format:" name="dateFormat" options={getDateFormatOptions()} validated={submitCount > 0} value={values.dateFormat} onChange={setFieldValue} />
                            <FormSelect invalid={!!errors.firstDayOfTheWeek} label="First day of the week:" name="firstDayOfTheWeek" options={getFirstDayOfTheWeekOptions()} validated={submitCount > 0} value={values.firstDayOfTheWeek} onChange={setFieldValue} />
                            <FormSelect invalid={!!errors.periodType} label="Period:" name="periodType" options={getPeriodType()} validated={submitCount > 0} value={values.periodType} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Specialities">
                            <FieldArray name="specialities">
                                {(arrayHelpers: FieldArrayRenderProps) => (
                                    <>
                                        <Table headers={[ "Title" ]} required={[ true ]}>
                                            {values.specialities.map((speciality: Speciality, index: number) =>
                                                <SpecialityFields errors={errors} index={index} key={speciality.id} setFieldValue={setFieldValue} submitCount={submitCount} values={values} />
                                            )}
                                        </Table>
                                        <Button text="Add Speciality" theme="success" onClick={() => arrayHelpers.push(addSpeciality())} />
                                    </>
                                )}
                            </FieldArray>
                        </FormSection>
                        <FormActions isSubmitting={isSubmitting} onCancel={onCancel} />
                    </FormWrapper>
                )}
            </Formik>
        </>
    );
};

export default EditSettings;
