import { FormikErrors, getIn } from "formik";
import React from "react";
import { FormInput, Icon } from "smart-tech-solutions-frontend-components";

import { User } from "../../domain/user";

export type SpecialityFieldsProps = {
    errors?: FormikErrors<User>;
    index: number;
    setFieldValue: (name: string, value: string | number | undefined) => void;
    submitCount: number;
    values: User;
};

const SpecialityFields = ({ errors, index, values, setFieldValue, submitCount }: SpecialityFieldsProps): JSX.Element => (
    <tr key={index}>
        <td>
            <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!getIn(errors, "specialities." + index + ".title")} name={`specialities.${index}.title`} theme="no-margins" type="text" validated={submitCount > 0} value={values.specialities[index].title} onChange={setFieldValue} />
        </td>
    </tr>
);

export default SpecialityFields;
