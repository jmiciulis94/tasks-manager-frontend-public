import React from "react";
import { Table, TableActions } from "smart-tech-solutions-frontend-components";

import { Employee, formattedTypeOfPay, fullName } from "../../domain/employee";

export type EmployeeTableProps = {
    employeeList: Employee[];
    onDelete: (id: number) => void;
};

const EmployeeTable = ({ employeeList, onDelete }: EmployeeTableProps): JSX.Element => (
    <Table headers={[ "No", "Full Name", "Email", "Phone", "Specialities", "Age", "Start date", "Type of pay", "Pay rate", "Actions" ]}>
        {employeeList.map((employee: Employee, i: number) => (
            <tr key={employee.id}>
                <td className="sts-table-numeric-column">{i + 1}</td>
                <td>{fullName(employee.firstName, employee.lastName)}</td>
                <td>{employee.email}</td>
                <td>{employee.phone}</td>
                <td>
                    {employee.specialityTitles && employee.specialityTitles.map((specialityTitle: string, i: number) => (
                        <React.Fragment key={specialityTitle}>
                            {i !== 0 && <br />}
                            {specialityTitle}
                        </React.Fragment>
                    ))}
                </td>
                <td>{employee.age}</td>
                <td>{employee.startDate}</td>
                <td>{formattedTypeOfPay(employee.typeOfPay)}</td>
                <td>{employee.payRate}</td>
                <TableActions editUrl={"/employee/" + employee.id + "?mode=edit"} id={employee.id || 0} viewUrl={"/employee/" + employee.id + "?mode=view"} onDelete={onDelete} />
            </tr>
        ))}
    </Table>
);

export default EmployeeTable;
