import React, { useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, Page, showDeleteAlert } from "smart-tech-solutions-frontend-components";
import { SweetAlertResult } from "sweetalert2";

import * as apiActions from "../../actions/apiActions";
import * as employeeActions from "../../actions/employeeActions";
import { RootState } from "../../reducers/initialState";

import EmployeeTable from "./EmployeeTable";

const mapStateToProps = (state: RootState) => {
    return {
        employeeList: state.employee.employeeList,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages
    };
};

const mapDispatchToProps = {
    deleteEmployee: employeeActions.deleteEmployee,
    getEmployeeList: employeeActions.getEmployeeList,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const EmployeeListPage = (props: PropsFromRedux): JSX.Element => {
    useEffect(() => {
        props.getEmployeeList();
    }, []);

    const handleDelete = async (id: number): Promise<void> => {
        const result: SweetAlertResult = await showDeleteAlert();
        if (result.value) {
            await props.deleteEmployee(id);
            window.scrollTo(0, 0);
        }
    };

    const renderNoEmployeesMessage = (): JSX.Element => (
        <>You don&#39;t have any employees yet.</>
    );

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Employees", url: "/employee-list" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Employees">
            {props.employeeList.length > 0 ? <EmployeeTable employeeList={props.employeeList} onDelete={handleDelete} /> : renderNoEmployeesMessage()}
        </Page>
    );
};

export default connector(EmployeeListPage);
