import React from "react";
import { FormSelectStateless } from "smart-tech-solutions-frontend-components";

import { getPeriodOptions, Period } from "../../domain/report";

type SelectPeriodType = {
    onPeriodChange: (periodId: string) => void;
    periodList: Period[];
    selectedPeriodId: string | null;
};

const SelectPeriod = ({ periodList, onPeriodChange, selectedPeriodId }: SelectPeriodType): JSX.Element => (
    <FormSelectStateless label="Period:" options={getPeriodOptions(periodList)} value={selectedPeriodId || ""} onChange={onPeriodChange} />
);

export default SelectPeriod;
