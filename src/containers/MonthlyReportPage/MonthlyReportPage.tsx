import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, Page } from "smart-tech-solutions-frontend-components";

import * as apiActions from "../../actions/apiActions";
import * as periodActions from "../../actions/periodActions";
import * as reportActions from "../../actions/reportActions";
import { RootState } from "../../reducers/initialState";

import MonthlyReportView from "./MonthlyReportView";
import SelectPeriod from "./SelectPeriod";

const mapStateToProps = (state: RootState) => {
    return {
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        monthlyReport: state.report.monthlyReport,
        period: state.period.period,
        periodList: state.period.periodList,
        successMessages: state.api.successMessages
    };
};

const mapDispatchToProps = {
    getMonthlyReport: reportActions.getMonthlyReport,
    getPeriod: periodActions.getPeriod,
    getPeriodList: periodActions.getPeriodList,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const MonthlyReportPage = (props: PropsFromRedux) => {

    const [ selectedPeriodId, setSelectedPeriodId ] = useState<number | null>(null);

    useEffect(() => {
        props.getPeriodList();
        setSelectedPeriodId(null);
    }, []);

    const handleEmployeeChange = (periodId: string) => {
        if (periodId) {
            setSelectedPeriodId(parseInt(periodId));
            props.getPeriod(parseInt(periodId));
            props.getMonthlyReport(parseInt(periodId));
        }
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Monthly report", url: "/monthly-report" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Monthly report">
            <SelectPeriod periodList={props.periodList} selectedPeriodId={selectedPeriodId ? selectedPeriodId.toString() : null} onPeriodChange={handleEmployeeChange} />
            {props.period && selectedPeriodId ? <MonthlyReportView monthlyReport={props.monthlyReport} period={props.period} /> : <>Please select a period to display its report table.</>}
        </Page>
    );
};

export default connector(MonthlyReportPage);
