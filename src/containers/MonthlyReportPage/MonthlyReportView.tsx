import React from "react";

import MonthlyReportTable from "../../components/MonthlyReportTable";
import { formattedDate, MonthlyReport, Period } from "../../domain/report";

export type MonthlyReportTableProps = {
    monthlyReport?: MonthlyReport;
    period: Period;
};

const MonthlyReportView = ({ monthlyReport, period }: MonthlyReportTableProps) : JSX.Element => {

    if (monthlyReport && monthlyReport.entries.length !== 0) {
        return (
            <>
                <MonthlyReportTable entries={monthlyReport.entries} tasks={monthlyReport.tasks} totals={monthlyReport.totals} />
            </>
        );
    } else {
        return (
            <>No time cards created for period: {formattedDate(period.year, period.month)}.</>
        );
    }
};

export default MonthlyReportView;
