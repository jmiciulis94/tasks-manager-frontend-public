import React from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, Page } from "smart-tech-solutions-frontend-components";

import * as apiActions from "../../actions/apiActions";
import { RootState } from "../../reducers/initialState";

const mapStateToProps = (state: RootState) => {
    return {
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages
    };
};

const mapDispatchToProps = {
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const HomePage = (props: PropsFromRedux): JSX.Element => {
    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Home">
            Coming soon.
        </Page>
    );
};

export default connector(HomePage);
