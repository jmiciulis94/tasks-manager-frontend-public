import React from "react";
import { Button, FormSection, FormValue, FormWrapper } from "smart-tech-solutions-frontend-components";

import { Employee, formattedPayPerHour, formattedSpecialities, formattedTypeOfPay, TYPE_OF_PAY_HOURLY, TYPE_OF_PAY_RATE } from "../../domain/employee";
import { Speciality } from "../../domain/speciality";

export type ViewEmployeeProps = {
    employee: Employee;
    onBack: () => void;
    onEdit: () => void;
    specialities: Speciality[];
};

const ViewEmployee = ({ employee, onBack, onEdit, specialities }: ViewEmployeeProps): JSX.Element => (
    <FormWrapper theme="view">
        <FormSection title="General">
            <FormValue label="First Name" value={employee.firstName} />
            <FormValue label="Last Name" value={employee.lastName} />
            <FormValue label="Date of Birth" value={employee.dateOfBirth} />
        </FormSection>
        <FormSection title="Contacts">
            <FormValue label="Email" value={employee.email} />
            <FormValue label="Phone Number" value={employee.phone} />
        </FormSection>
        <FormSection title="Job">
            <FormValue label="Start Date" value={employee.startDate} />
            <FormValue label="Specialities" value={employee.specialityIds && formattedSpecialities(employee.specialityIds, specialities)} />
            <FormValue label="Type of Pay" value={formattedTypeOfPay(employee.typeOfPay)} />
            {employee.typeOfPay === TYPE_OF_PAY_RATE && <FormValue label="Rate" value={employee.rate} />}
            {employee.typeOfPay === TYPE_OF_PAY_HOURLY && <FormValue label="Paid per hour" value={formattedPayPerHour(employee.payPerHour)} />}
        </FormSection>
        <Button text="Edit" theme="success" onClick={onEdit} />
        <Button text="Back" theme="secondary" onClick={onBack} />
    </FormWrapper>
);

export default ViewEmployee;
