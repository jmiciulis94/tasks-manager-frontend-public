import { FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router";
import { BreadcrumbEntry, FormOption, Page } from "smart-tech-solutions-frontend-components";

import * as apiActions from "../../actions/apiActions";
import * as employeeActions from "../../actions/employeeActions";
import * as specialityActions from "../../actions/specialityActions";
import { Employee, fullName, TYPE_OF_PAY_RATE } from "../../domain/employee";
import { getSpecialityOptions } from "../../domain/speciality";
import { RootState } from "../../reducers/initialState";

import EditEmployee from "./EditEmployee";
import ViewEmployee from "./ViewEmployee";

export type EmployeePageProps = {
    id: string;
};

const mapStateToProps = (state: RootState) => {
    return {
        employee: state.employee.employee,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        specialityList: state.speciality.specialityList,
        successMessages: state.api.successMessages,
        validationError: state.api.validationError
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    getEmployee: employeeActions.getEmployee,
    getSpecialityList: specialityActions.getSpecialityList,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveEmployee: employeeActions.saveEmployee
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type AllEmployeePageProps = RouteComponentProps<EmployeePageProps> & PropsFromRedux;

const emptyEmployee: Employee = {
    firstName: "",
    specialityIds: [],
    typeOfPay: TYPE_OF_PAY_RATE
};

const EmployeePage = (props: AllEmployeePageProps): JSX.Element => {
    const [ employee, setEmployee ] = useState<Employee>(emptyEmployee);
    const [ specialityOptions, setSpecialityOptions ] = useState<FormOption[]>([]);
    const [ editMode, setEditMode ] = useState<boolean>(false);

    useEffect(() => {
        props.getSpecialityList();
    }, []);

    useEffect(() => {
        if (props.employee) {
            setEmployee(props.employee);
        }
    }, [ props.employee ]);

    useEffect(() => {
        setSpecialityOptions(getSpecialityOptions(props.specialityList));
    }, [ props.specialityList ]);

    useEffect(() => {
        const params: URLSearchParams = new URLSearchParams(props.location.search);
        const mode: string | null = params.get("mode");
        const id: number = parseInt(props.match.params.id);
        props.clearValidationError();

        if (id) {
            props.getEmployee(id);
            mode && mode === "edit" ? setEditMode(true) : setEditMode(false);
        } else {
            setEmployee(emptyEmployee);
            setEditMode(true);
        }
    }, [ props.match.params ]);

    const handleSubmit = async (employee: Employee, actions: FormikHelpers<Employee>): Promise<void> => {
        const success: boolean = await props.saveEmployee(employee);

        if (success) {
            actions.resetForm();
            props.clearValidationError();
            props.history.goBack();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleBack = (): void => {
        props.history.goBack();
    };

    const handleEdit = (): void => {
        props.history.push("/employee/" + props.match.params.id + "?mode=edit");
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Employees", url: "/employee-list" };
    const breadcrumbPage3: BreadcrumbEntry = { text: fullName(employee.firstName, employee.lastName), url: "/blinds/" + employee.id };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2, breadcrumbPage3 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title={fullName(employee.firstName, employee.lastName)}>
            {editMode ? <EditEmployee employee={employee} specialityOptions={specialityOptions} validationError={props.validationError} onCancel={handleBack} onSubmit={handleSubmit} /> : <ViewEmployee employee={employee} specialities={props.specialityList} onBack={handleBack} onEdit={handleEdit} />}
        </Page>
    );
};

export default connector(EmployeePage);
