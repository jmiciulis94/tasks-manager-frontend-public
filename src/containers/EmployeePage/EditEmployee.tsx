import { Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { FormActions, FormDatePicker, FormInput, FormMultiSelect, FormOption, FormRadioButton, FormSection, FormValidationErrors, FormWrapper, Icon } from "smart-tech-solutions-frontend-components";
import { array, number, object, SchemaOf, string } from "yup";

import { Employee, getTypeOfPayOptions, TYPE_OF_PAY_HOURLY, TYPE_OF_PAY_NONE, TYPE_OF_PAY_RATE } from "../../domain/employee";
import { ValidationError } from "../../utils/apiUtils";

export type EditEmployeeProps = {
    employee: Employee;
    onCancel: () => void;
    onSubmit: (values: Employee, actions: FormikHelpers<Employee>) => void;
    specialityOptions: FormOption[];
    validationError?: ValidationError;
};

const employeeValidationSchema: SchemaOf<Employee> = object({
    dateOfBirth: string().notRequired().nullable(),
    email: string().notRequired().nullable().max(100, "Can not be longer than 100 symbols"),
    firstName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    lastName: string().notRequired().nullable().max(100, "Can not be longer than 100 symbols"),
    payPerHour: number().nullable().when("typeOfPay", {
        is: TYPE_OF_PAY_HOURLY,
        otherwise: number().notRequired(),
        then: number().typeError("Must be a number").required("This field is required").positive("Must be positive number")
    }),
    phone: string().notRequired().nullable().max(20, "Can not be longer than 20 symbols"),
    rate: number().nullable().when("typeOfPay", {
        is: TYPE_OF_PAY_RATE,
        otherwise: number().notRequired(),
        then: number().typeError("Must be a number").required("This field is required").min(0, "Must be between 0 and 1").max(1, "Must be between 0 and 1")
    }),
    specialityIds: array().of(number()),
    startDate: string().notRequired().nullable(),
    typeOfPay: string().required("This field is required")
});

const EditEmployee = ({ employee, onCancel, onSubmit, specialityOptions, validationError }: EditEmployeeProps): JSX.Element => {
    const clearUnusedTypeOfPayFields = (values: Employee) => {
        if (values.typeOfPay === TYPE_OF_PAY_NONE && (values.rate != undefined || values.payPerHour != undefined)) {
            values.payPerHour = undefined;
            values.rate = undefined;
        } else if (values.typeOfPay === TYPE_OF_PAY_HOURLY && values.rate != undefined) {
            values.rate = undefined;
        } else if (values.typeOfPay === TYPE_OF_PAY_RATE && values.payPerHour != undefined) {
            values.payPerHour = undefined;
        }
    };

    return (
        <>
            <FormValidationErrors validationError={validationError} />
            <Formik enableReinitialize={true} initialValues={employee} validateOnBlur={false} validateOnChange={false} validationSchema={employeeValidationSchema} onSubmit={onSubmit}>
                {({ values, errors, isSubmitting, setFieldValue, submitCount }: FormikProps<Employee>) => (
                    <FormWrapper>
                        {clearUnusedTypeOfPayFields(values)}
                        <FormSection title="General">
                            <FormInput icon={<Icon iconClass="user" type="fa" />} invalid={!!errors.firstName} label="First Name" name="firstName" required={true} type="text" validated={submitCount > 0} value={values.firstName} onChange={setFieldValue} />
                            <FormInput icon={<Icon iconClass="user" type="fa" />} invalid={!!errors.lastName} label="Last Name" name="lastName" type="text" validated={submitCount > 0} value={values.lastName} onChange={setFieldValue} />
                            <FormDatePicker icon={<Icon iconClass="calendar-week" type="fa" />} invalid={!!errors.dateOfBirth} label="Date of Birth" name="dateOfBirth" validated={submitCount > 0} value={values.dateOfBirth} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Contacts">
                            <FormInput icon={<Icon iconClass="envelope" type="fa" />} invalid={!!errors.email} label="Email" name="email" type="email" validated={submitCount > 0} value={values.email} onChange={setFieldValue} />
                            <FormInput icon={<Icon iconClass="phone" type="fa" />} invalid={!!errors.phone} label="Phone Number" name="phone" type="tel" validated={submitCount > 0} value={values.phone} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Job">
                            <FormDatePicker icon={<Icon iconClass="calendar-week" type="fa" />} invalid={!!errors.startDate} label="Start Date" name="startDate" validated={submitCount > 0} value={values.startDate} onChange={setFieldValue} />
                            <FormMultiSelect invalid={!!errors.specialityIds} label="Specialities" name="specialityIds" options={specialityOptions} validated={submitCount > 0} onChange={setFieldValue} />
                            <FormRadioButton invalid={!!errors.typeOfPay} label="Type of Pay" name="typeOfPay" options={getTypeOfPayOptions()} required={true} validated={submitCount > 0} value={values.typeOfPay} onChange={setFieldValue} />
                            {values.typeOfPay === TYPE_OF_PAY_RATE && <FormInput iconLabel="0..1" invalid={!!errors.rate} label="Rate" name="rate" required={true} type="text" validated={submitCount > 0} value={values.rate} onChange={setFieldValue} />}
                            {values.typeOfPay === TYPE_OF_PAY_HOURLY && <FormInput icon={<Icon iconClass="dollar-sign" type="fa" />} invalid={!!errors.payPerHour} label="Paid per Hour" name="payPerHour" required={true} type="number" validated={submitCount > 0} value={values.payPerHour} onChange={setFieldValue} />}
                        </FormSection>
                        <FormActions isSubmitting={isSubmitting} onCancel={onCancel} />
                    </FormWrapper>
                )}
            </Formik>
        </>
    );
};

export default EditEmployee;
