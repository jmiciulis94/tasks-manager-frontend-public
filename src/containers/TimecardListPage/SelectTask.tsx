import { FormikErrors } from "formik";
import React from "react";
import { FormOption, FormSelect } from "smart-tech-solutions-frontend-components";

import { Timecard } from "../../domain/timecard";

type SelectTaskType = {
    errors: FormikErrors<Timecard>;
    setFieldValue: (name: string, value: string | number | boolean | undefined) => void;
    taskOptions: FormOption[];
    values: Timecard;
};

const SelectTask = ({ errors, setFieldValue, taskOptions, values }: SelectTaskType): JSX.Element => {
    if (taskOptions.length !== 0) {
        return (
            <FormSelect invalid={!!errors.taskId} label="Task" name="taskId" options={taskOptions} required={true} theme="floating" value={values.taskId} onChange={setFieldValue} />
        );
    } else {
        return (
            <></>
        );
    }
};

export default SelectTask;
