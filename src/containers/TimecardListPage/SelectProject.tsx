import { FormikErrors } from "formik";
import React, { useEffect } from "react";
import { FormSelect } from "smart-tech-solutions-frontend-components";
import { FormOption } from "smart-tech-solutions-frontend-components/lib/FormWrapper";

import { Timecard } from "../../domain/timecard";

type SelectProjectType = {
    errors: FormikErrors<Timecard>;
    onProjectChange: (projectId: number) => void;
    projectOptions: FormOption[];
    setFieldValue: (name: string, value: string | number | boolean | undefined) => void;
    values: Timecard;
};

const SelectProject = ({ errors, onProjectChange, projectOptions, setFieldValue, values }: SelectProjectType): JSX.Element => {

    useEffect(() => {
        if (values.projectId) {
            onProjectChange(values.projectId);
        }
    }, [ values.projectId ]);

    return (
        <FormSelect invalid={!!errors.projectId} label="Project" name="projectId" options={projectOptions} required={true} theme="floating" value={values.projectId} onChange={setFieldValue} />
    );
};

export default SelectProject;
