import { FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router";
import { BreadcrumbEntry, FormDatePickerStateless, FormOption, Page, showDeleteAlert } from "smart-tech-solutions-frontend-components";
import { SweetAlertResult } from "sweetalert2";

import * as apiActions from "../../actions/apiActions";
import * as employeeActions from "../../actions/employeeActions";
import * as projectActions from "../../actions/projectActions";
import * as timecardActions from "../../actions/timecardActions";
import SelectEmployee from "../../components/SelectEmployee";
import { getEmployeeOptions } from "../../domain/employee";
import { getProjectOptions, getTaskOptions, Project } from "../../domain/project";
import { Timecard } from "../../domain/timecard";
import { RootState } from "../../reducers/initialState";
import { currentDate } from "../../utils/dateUtils";

import EditTimecard from "./EditTimecard";
import TimecardTable from "./TimecardTable";

const mapStateToProps = (state: RootState) => {
    return {
        employeeList: state.employee.employeeList,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        projectList: state.project.projectList,
        successMessages: state.api.successMessages,
        timecard: state.timecard.timecard,
        timecardList: state.timecard.timecardList,
        validationError: state.api.validationError
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    deleteTimecard: timecardActions.deleteTimecard,
    getEmployeeList: employeeActions.getEmployeeList,
    getProjectList: projectActions.getProjectList,
    getTimecard: timecardActions.getTimecard,
    getTimecardList: timecardActions.getTimecardList,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveTimecard: timecardActions.saveTimecard
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type AllTimecardPageProps = RouteComponentProps & PropsFromRedux;

const emptyTimecard: Timecard = {
    date: "",
    employeeId: 0,
    minutes: 0,
    projectId: 0,
    taskId: 0
};

const TimecardListPage = (props: AllTimecardPageProps): JSX.Element => {

    const [ timecard, setTimecard ] = useState<Timecard>(emptyTimecard);
    const [ selectedEmployeeId, setSelectedEmployeeId ] = useState<number | null>(null);
    const [ selectedMonth, setSelectedMonth ] = useState<string | null>(null);
    const [ employeeOptions, setEmployeeOptions ] = useState<FormOption[]>([]);
    const [ projectOptions, setProjectOptions ] = useState<FormOption[]>([]);
    const [ taskOptions, setTaskOptions ] = useState<FormOption[]>([]);
    const [ selectedProjectId, setSelectedProjectId ] = useState<number | null>(null);

    useEffect(() => {
        props.getEmployeeList();
        props.getProjectList();
        setSelectedEmployeeId(null);
        setSelectedMonth(currentDate);
    }, []);

    useEffect(() => {
        if (props.timecard) {
            setTimecard(props.timecard);
        }
    }, [ props.timecard ]);

    useEffect(() => {
        setEmployeeOptions(getEmployeeOptions(props.employeeList));
    }, [ props.employeeList ]);

    useEffect(() => {
        setProjectOptions(getProjectOptions(props.projectList));
    }, [ props.projectList ]);

    useEffect(() => {
        const selectedProject: Project | undefined = props.projectList.find((p: Project) => selectedProjectId == p.id);
        if (selectedProject) {
            setTaskOptions(getTaskOptions(selectedProject.tasks));
        }
    }, [ selectedProjectId ]);

    const handleEmployeeChange = (employeeId: string | null): void => {
        if (employeeId) {
            setSelectedEmployeeId(parseInt(employeeId));
            props.getTimecardList(parseInt(employeeId));
        }
    };

    const handleProjectChange = (projectId: number): void => {
        if (projectId) {
            setSelectedProjectId(projectId);
        }
    };

    const handleDatePickerChange = (date: string | null): void => {
        if (date) {
            setSelectedMonth(date);
        }
    };

    const handleSubmit = async (timecard: Timecard, actions: FormikHelpers<Timecard>): Promise<void> => {
        const success: boolean = await props.saveTimecard(timecard);
        if (success) {
            actions.resetForm();
            props.clearValidationError();
        } else {
            // toDo: implement after we have a decent react-bootstrap modal working
        }
    };

    const handleDelete = async (id: number): Promise<void> => {
        const result: SweetAlertResult = await showDeleteAlert();
        if (result.value) {
            await props.deleteTimecard(id);
            window.scrollTo(0, 0);
        }
    };

    const renderMessage = (): JSX.Element => (
        <>Select an employee.</>
    );

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Timecards", url: "/timecard-list" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Timecards">
            <EditTimecard employeeOptions={employeeOptions} projectOptions={projectOptions} taskOptions={taskOptions} timecard={timecard} onProjectChange={handleProjectChange} onSubmit={handleSubmit} />
            <SelectEmployee employeeList={props.employeeList} selectedEmployeeId={selectedEmployeeId ? selectedEmployeeId.toString() : null} onEmployeeChange={handleEmployeeChange} />
            <FormDatePickerStateless label="Month:" mode="month" value={selectedMonth} onChange={handleDatePickerChange} />
            {props.timecardList.length > 0 && selectedEmployeeId ? <TimecardTable selectedMonth={selectedMonth} timecardList={props.timecardList} onDelete={handleDelete} /> : renderMessage()}
        </Page>
    );
};

export default connector(TimecardListPage);
