import { Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { FormArea, FormDatePicker, FormInput, FormOption, FormSection, FormSelect, FormValidationErrors, FormWrapper, Icon } from "smart-tech-solutions-frontend-components";
import { number, object, SchemaOf, string } from "yup";

import FormModal from "../../components/FormModal";
import { Timecard } from "../../domain/timecard";
import { ValidationError } from "../../utils/apiUtils";

import SelectProject from "./SelectProject";
import SelectTask from "./SelectTask";

export type EditTimecardProps = {
    employeeOptions: FormOption[];
    onProjectChange: (projectId: number) => void;
    onSubmit: (values: Timecard, actions: FormikHelpers<Timecard>) => void;
    projectOptions: FormOption[];
    taskOptions: FormOption[];
    timecard: Timecard;
    validationError?: ValidationError;
};

const timecardValidationSchema: SchemaOf<Timecard> = object({
    date: string().required("This field is required"),
    description: string().notRequired().nullable(),
    employeeId: number().required("You must select an employee"),
    minutes: number().typeError("Must be a number").required("This field is required").positive("Must be a positive number"),
    projectId: number().required("You must select a project"),
    taskId: number().required("You must select a task")
});

const EditTimecard = ({ timecard, onProjectChange, onSubmit, employeeOptions, projectOptions, taskOptions, validationError }: EditTimecardProps): JSX.Element => {
    return (
        <>
            <FormValidationErrors validationError={validationError} />
            <Formik enableReinitialize={true} initialValues={timecard} validateOnBlur={false} validateOnChange={false} validationSchema={timecardValidationSchema} onSubmit={onSubmit}>
                {({ values, errors, isSubmitting, setFieldValue, submitCount }: FormikProps<Timecard>) => (
                    <FormWrapper>
                        <FormModal id="create-timecard" isSubmitting={isSubmitting} title="Create New Timecard">
                            <FormSection title="Select options">
                                <FormSelect invalid={!!errors.employeeId} label="Employee" name="employeeId" options={employeeOptions} required={true} theme="floating" validated={submitCount > 0} value={values.employeeId} onChange={setFieldValue} />
                                <SelectProject errors={errors} projectOptions={projectOptions} setFieldValue={setFieldValue} values={values} onProjectChange={onProjectChange} />
                                <SelectTask errors={errors} setFieldValue={setFieldValue} taskOptions={taskOptions} values={values} />
                            </FormSection>
                            <FormSection title="Date and time">
                                <FormDatePicker icon={<Icon iconClass="calendar-week" type="fa" />} invalid={!!errors.date} label="Date" name="date" required={true} validated={submitCount > 0} value={values.date} onChange={setFieldValue} />
                                <FormInput icon={<Icon iconClass="clock" type="fa" />} invalid={!!errors.minutes} label="Minutes" name="minutes" required={true} type="number" validated={submitCount > 0} value={values.minutes} onChange={setFieldValue}/>
                                <FormArea invalid={!!errors.description} label="Description" name="description" rows={5} validated={submitCount > 0} value={values.description} onChange={setFieldValue} />
                            </FormSection>
                        </FormModal>
                    </FormWrapper>
                )}
            </Formik>
        </>
    );
};

export default EditTimecard;
