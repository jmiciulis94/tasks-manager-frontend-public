import React from "react";
import { Table } from "smart-tech-solutions-frontend-components";

import { Timecard } from "../../domain/timecard";
import { daysInMonth, dayToDate } from "../../utils/dateUtils";

import RenderTimecardRow from "./RenderTimecardRow";

export type TimecardTableProps = {
    onDelete: (id: number) => void;
    selectedMonth: string | null;
    timecardList: Timecard[];
};

const TimecardTable = ({ timecardList, selectedMonth, onDelete }: TimecardTableProps): JSX.Element => {

    const timecardRows: JSX.Element[] = [];
    if (selectedMonth !== null) {
        for (let i = 0; i < daysInMonth(selectedMonth); i++) {
            const dayTimecards = timecardList.filter((t: Timecard) => t.date == dayToDate(i + 1, selectedMonth).toString());
            dayTimecards.forEach((timecard: Timecard, index: number) => {
                if (index === 0) {
                    timecard.id && timecardRows.push(
                        <RenderTimecardRow index={timecard.id} key={timecard.id} numberOfTasksThisDay={dayTimecards.length} skipDate={false} timecard={timecard} onDelete={onDelete} />
                    );
                } else {
                    timecard.id && timecardRows.push(
                        <RenderTimecardRow index={timecard.id} key={timecard.id} numberOfTasksThisDay={dayTimecards.length} skipDate={true} timecard={timecard} onDelete={onDelete} />
                    );
                }
            });
            if (dayTimecards.length === 0) {
                timecardRows.push(
                    <tr key={i + 1 + "day"}>
                        <td>{dayToDate(i + 1, selectedMonth)}</td>
                        <td />
                        <td />
                        <td />
                        <td />
                    </tr>
                );
            }
        }
    }

    return (
        <Table headers={[ "Date", "Task", "Description", "Time", "Actions" ]}>
            {timecardRows}
        </Table>
    );
};

export default TimecardTable;
