import React from "react";
import { TableActions } from "smart-tech-solutions-frontend-components";

import { Timecard } from "../../domain/timecard";

export type RenderTimecardsProps = {
    index: number;
    numberOfTasksThisDay: number;
    onDelete: (id: number) => void;
    skipDate: boolean;
    timecard: Timecard;
};

const RenderTimecardRow = ({ index, numberOfTasksThisDay, skipDate, timecard, onDelete }: RenderTimecardsProps): JSX.Element => {

    return (
        <tr key={index}>
            {skipDate ? <></> : <td rowSpan={numberOfTasksThisDay}>{timecard.date}</td>}
            <td>{timecard.taskTitle}</td>
            <td>{timecard.description}</td>
            <td>{timecard.minutes}</td>
            {timecard.id && <TableActions editUrl={"/timecard/" + timecard.id + "?mode=edit"} id={timecard.id} viewUrl={"/timecard/" + timecard.id + "?mode=view"} onDelete={onDelete} />}
        </tr>
    );
};

export default RenderTimecardRow;
