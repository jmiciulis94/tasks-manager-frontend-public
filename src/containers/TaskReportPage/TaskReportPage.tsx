import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, FormOption, Page } from "smart-tech-solutions-frontend-components";

import * as apiActions from "../../actions/apiActions";
import * as projectActions from "../../actions/projectActions";
import * as reportActions from "../../actions/reportActions";
import * as taskActions from "../../actions/taskActions";
import { getTaskOptions, Project } from "../../domain/project";
import { RootState } from "../../reducers/initialState";

import SelectProject from "./SelectProject";
import SelectTask from "./SelectTask";
import TaskReportView from "./TaskReportView";

const mapStateToProps = (state: RootState) => {
    return {
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        projectList: state.project.projectList,
        successMessages: state.api.successMessages,
        task: state.task.task,
        taskList: state.task.taskList,
        taskReport: state.report.taskReport
    };
};

const mapDispatchToProps = {
    getProjectList: projectActions.getProjectList,
    getTask: taskActions.getTask,
    getTaskList: taskActions.getTaskList,
    getTaskReport: reportActions.getTaskReport,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const TaskReportPage = (props: PropsFromRedux) => {

    const [ selectedProjectId, setSelectedProjectId ] = useState<number | null>(null);
    const [ selectedTaskId, setSelectedTaskId ] = useState<number | null>(null);
    const [ isProjectSelected, setIsProjectSelected ] = useState<boolean>(false);
    const [ taskOptions, setTaskOptions ] = useState<FormOption[]>([]);

    useEffect(() => {
        props.getProjectList();
        setSelectedProjectId(null);
        setSelectedTaskId(null);
        setIsProjectSelected(false);
    }, []);

    useEffect(() => {
        if (selectedProjectId) {
            const selectedProject: Project | undefined = props.projectList.find((p : Project) => selectedProjectId == p.id);
            if (selectedProject) {
                setTaskOptions(getTaskOptions(selectedProject.tasks));
            }
            setIsProjectSelected(true);
        }
    }, [ selectedProjectId ]);

    const handleProjectChange = (projectId: string) => {
        if (projectId) {
            setSelectedProjectId(parseInt(projectId));
        }
    };

    const handleTaskChange = (taskId: string) => {
        if (taskId) {
            setSelectedTaskId(parseInt(taskId));
            props.getTask(parseInt(taskId));
            props.getTaskReport(parseInt(taskId));
        }
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Task report", url: "/task-report" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Task report">
            <SelectProject projectList={props.projectList} selectedProjectId={selectedProjectId ? selectedProjectId.toString() : null} onProjectChange={handleProjectChange} />
            {isProjectSelected ? <SelectTask selectedTaskId={selectedTaskId ? selectedTaskId.toString() : null} taskOptions={taskOptions} onTaskChange={handleTaskChange} /> : <>Please select a project.</>}
            {props.task && isProjectSelected ? <TaskReportView task={props.task} taskReport={props.taskReport} /> : isProjectSelected && props.taskList.length !== 0 ? <>Please select a task to display its report table.</> : <></>}
        </Page>
    );
};

export default connector(TaskReportPage);
