import React from "react";
import { FormOption, FormSelectStateless } from "smart-tech-solutions-frontend-components";

type SelectTaskType = {
    onTaskChange: (taskId: string) => void;
    selectedTaskId: string | null;
    taskOptions: FormOption[];
};

const SelectTask = ({ taskOptions, onTaskChange, selectedTaskId }: SelectTaskType): JSX.Element => {
    if (taskOptions.length !== 0) {
        return (
            <FormSelectStateless label="Task:" options={taskOptions} value={selectedTaskId || ""} onChange={onTaskChange} />
        );
    } else {
        return (
            <>Selected project does not have any tasks created.</>
        );
    }
};

export default SelectTask;
