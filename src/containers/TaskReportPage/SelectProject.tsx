import React from "react";
import { FormSelectStateless } from "smart-tech-solutions-frontend-components";

import { getProjectOptions, Project } from "../../domain/project";

type SelectProjectType = {
    onProjectChange: (projectId: string) => void;
    projectList: Project[];
    selectedProjectId: string | null;
};

const SelectProject = ({ projectList, onProjectChange, selectedProjectId }: SelectProjectType): JSX.Element => (
    <FormSelectStateless label="Project:" options={getProjectOptions(projectList)} value={selectedProjectId || ""} onChange={onProjectChange} />
);

export default SelectProject;
