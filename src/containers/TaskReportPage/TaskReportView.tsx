import React from "react";

import TaskReportTable from "../../components/TaskReportTable";
import { Task } from "../../domain/project";
import { TaskReport } from "../../domain/report";

export type TaskReportTableProps = {
    task: Task;
    taskReport?: TaskReport;
};

const TaskReportView = ({ taskReport, task }: TaskReportTableProps) : JSX.Element => {

    if (taskReport && taskReport.entries.length !== 0) {
        return (
            <>
                <TaskReportTable employees={taskReport.employees} entries={taskReport.entries} totals={taskReport.totals} />
            </>
        );
    } else {
        return (
            <>No time cards created for task: {task.title}.</>
        );
    }
};

export default TaskReportView;
