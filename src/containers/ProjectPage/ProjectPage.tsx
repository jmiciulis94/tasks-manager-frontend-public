import { FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router";
import { BreadcrumbEntry, Page } from "smart-tech-solutions-frontend-components";

import * as apiActions from "../../actions/apiActions";
import * as projectActions from "../../actions/projectActions";
import { Project } from "../../domain/project";
import { RootState } from "../../reducers/initialState";

import EditProject from "./EditProject";
import ViewProject from "./ViewProject";

export type ProjectPageProps = {
    id: string;
};

const mapStateToProps = (state: RootState) => {
    return {
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        project: state.project.project,
        successMessages: state.api.successMessages,
        validationError: state.api.validationError
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    getProject: projectActions.getProject,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveProject: projectActions.saveProject
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type AllProjectPageProps = RouteComponentProps<ProjectPageProps> & PropsFromRedux;

const emptyProject: Project = {
    tasks: [],
    title: ""
};

const ProjectPage = (props: AllProjectPageProps): JSX.Element => {
    const [ project, setProject ] = useState<Project>(emptyProject);
    const [ editMode, setEditMode ] = useState<boolean>(false);

    useEffect(() => {
        if (props.project) {
            setProject(props.project);
        }
    }, [ props.project ]);

    useEffect(() => {
        const params: URLSearchParams = new URLSearchParams(props.location.search);
        const mode: string | null = params.get("mode");
        const id: number = parseInt(props.match.params.id);
        props.clearValidationError();

        if (id) {
            props.getProject(id);
            mode && mode === "edit" ? setEditMode(true) : setEditMode(false);
        } else {
            setProject(emptyProject);
            setEditMode(true);
        }
    }, [ props.match.params ]);

    const handleSubmit = async (project: Project, actions: FormikHelpers<Project>): Promise<void> => {

        const success: boolean = await props.saveProject(project);

        if (success) {
            actions.resetForm();
            props.clearValidationError();
            props.history.goBack();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleBack = (): void => {
        props.history.goBack();
    };

    const handleEdit = (): void => {
        props.history.push("/project/" + props.match.params.id + "?mode=edit");
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Projects", url: "/project-list" };
    const breadcrumbPage3: BreadcrumbEntry = { text: project.title, url: "/project/" + project.id };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2, breadcrumbPage3 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title={project.title}>
            {editMode ? <EditProject project={project} validationError={props.validationError} onCancel={handleBack} onSubmit={handleSubmit} /> : <ViewProject project={project} onBack={handleBack} onEdit={handleEdit} />}
        </Page>
    );
};

export default connector(ProjectPage);
