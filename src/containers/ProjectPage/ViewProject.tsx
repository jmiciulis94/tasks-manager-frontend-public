import React from "react";
import { Button, FormSection, FormValue, FormWrapper, Table } from "smart-tech-solutions-frontend-components";

import { Project, Task } from "../../domain/project";

export type ViewProjectProps = {
    onBack: () => void;
    onEdit: () => void;
    project: Project;
};

const ViewProject = ({ onBack, onEdit, project }: ViewProjectProps): JSX.Element => (
    <FormWrapper theme="view">
        <FormSection title="General">
            <FormValue label="Title" value={project.title} />
            <FormValue label="Description" value={project.description} />
            <FormValue label="Estimated Cost" value={project.estimatedCost} />
            <FormValue label="Start Date" value={project.startDate} />
            <FormValue label="Estimated End Date" value={project.estimatedEndDate} />
        </FormSection>
        <FormSection title="Tasks">
            <Table headers={[ "Title", "Estimated Cost", "Start Date", "Estimated End Date" ]}>
                {project.tasks.map((task: Task) =>
                    <tr key={task.id}>
                        <td>{task.title}</td>
                        <td>{task.isPaidByFact ? task.isPaidByFact : task.estimatedCost}</td>
                        <td>{task.startDate}</td>
                        <td>{task.estimatedEndDate}</td>
                    </tr>
                )}
            </Table>
        </FormSection>
        <Button text="Edit" theme="success" onClick={onEdit} />
        <Button text="Back" theme="secondary" onClick={onBack} />
    </FormWrapper>
);

export default ViewProject;
