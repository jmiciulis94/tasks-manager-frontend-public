import { FieldArray, FieldArrayRenderProps, Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { Button, FormActions, FormDatePicker, FormInput, FormSection, FormValidationErrors, FormWrapper, Icon, Table } from "smart-tech-solutions-frontend-components";
import { array, boolean, number, object, SchemaOf, string } from "yup";

import { PeriodBudget, Project, Task } from "../../domain/project";
import { ValidationError } from "../../utils/apiUtils";

import TaskFields from "./TaskFields";

export type EditProjectProps = {
    onCancel: () => void;
    onSubmit: (values: Project, actions: FormikHelpers<Project>) => void;
    project: Project;
    validationError?: ValidationError;
};

const projectValidationSchema: SchemaOf<Project> = object({
    description: string().notRequired().nullable().max(500, "Can not be longer than 500 symbols"),
    estimatedCost: number().notRequired().nullable(),
    estimatedEndDate: string().notRequired().nullable(),
    startDate: string().required("This field is required"),
    tasks: array().of(
        object<Task>().shape({
            estimatedCost: number().nullable(),
            estimatedEndDate: string().nullable().when("isPaidByFact", {
                is: false,
                otherwise: string().notRequired(),
                then: string().required("This field is required")
            }),
            isPaidByFact: boolean().required("This field is required"),
            periodBudgets: array().of(
                object<PeriodBudget>().shape({
                    amount: number().typeError("Must be a number").required("This field is required").min(0, "Cannot be negative number"),
                    id: number().notRequired(),
                    month: number().notRequired().nullable(),
                    week: number().notRequired().nullable(),
                    year: number().required().positive()
                })
            ),
            startDate: string().nullable().when("isPaidByFact", {
                is: false,
                otherwise: string().notRequired(),
                then: string().required("This field is required")
            }),
            title: string().required("This field is required").max(100, "Can not be longer than 100 symbols")
        }).required("At least one task is required")
    ),
    title: string().required("This field is required").max(100, "Can not be longer than 100 symbols")
});

const EditProject = ({ onCancel, onSubmit, project, validationError }: EditProjectProps): JSX.Element => {
    const addTask = (): Task => {
        return {
            estimatedCost: 0,
            estimatedEndDate: new Date().toISOString(),
            isPaidByFact: false,
            periodBudgets: [],
            startDate: new Date().toISOString(),
            title: ""
        };
    };

    return (
        <>
            <FormValidationErrors validationError={validationError} />
            <Formik enableReinitialize={true} initialValues={project} validateOnBlur={false} validateOnChange={false} validationSchema={projectValidationSchema} onSubmit={onSubmit}>
                {({ values, errors, isSubmitting, setFieldValue, submitCount }: FormikProps<Project>) => (
                    <FormWrapper>
                        <FormSection title="General">
                            <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.title} label="Title" name="title" required={true} type="text" validated={submitCount > 0} value={values.title} onChange={setFieldValue} />
                            <FormInput icon={<Icon iconClass="pencil" type="fa" />} invalid={!!errors.description} label="Description" name="description" type="text" validated={submitCount > 0} value={values.description} onChange={setFieldValue} />
                            <FormInput disabled={true} icon={<Icon iconClass="dollar-sign" type="fa" />} invalid={!!errors.estimatedCost} label="Estimated Cost" name="estimatedCost" type="number" validated={submitCount > 0} value={values.estimatedCost} onChange={setFieldValue} />
                            <FormDatePicker icon={<Icon iconClass="calendar-week" type="fa" />} invalid={!!errors.startDate} label="Start Date" name="startDate" validated={submitCount > 0} value={values.startDate} onChange={setFieldValue} />
                            <FormDatePicker icon={<Icon iconClass="calendar-week" type="fa" />} invalid={!!errors.estimatedEndDate} label="Estimated End Date" name="estimatedEndDate" validated={submitCount > 0} value={values.estimatedEndDate} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Tasks">
                            <FieldArray name="tasks">
                                {(arrayHelpers: FieldArrayRenderProps) => (
                                    <>
                                        <Table headers={[ "Title", "Paid by Fact", "Start Date", "Estimated End Date", "Estimated Cost", "Monthly Budgets" ]} required={[ true, true, false, false, false ]}>
                                            {values.tasks.map((task: Task, index: number) =>
                                                <TaskFields errors={errors} index={index} key={task.id} setFieldValue={setFieldValue} submitCount={submitCount} values={values} />
                                            )}
                                        </Table>
                                        <Button text="Add Task" theme="success" onClick={() => arrayHelpers.push(addTask())} />
                                    </>
                                )}
                            </FieldArray>
                        </FormSection>
                        <FormActions isSubmitting={isSubmitting} onCancel={onCancel} />
                    </FormWrapper>
                )}
            </Formik>
        </>
    );
};

export default EditProject;
