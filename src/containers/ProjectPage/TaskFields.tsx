import { FormikErrors, getIn } from "formik";
import React, { useEffect } from "react";
import { FormDatePicker, FormInput, FormSelect, Icon } from "smart-tech-solutions-frontend-components";

import { formattedPeriod, getPeriodsForms, PeriodBudget, Project, Task } from "../../domain/project";
import { getYesNoOptions } from "../../utils/formUtils";

export type TaskFieldsProps = {
    errors?: FormikErrors<Project>;
    index: number;
    setFieldValue: (name: string, value: string | number | boolean | PeriodBudget[] | null | undefined) => void;
    submitCount: number;
    values: Project;
};

const TaskFields = ({ errors, index, setFieldValue, submitCount, values }: TaskFieldsProps): JSX.Element => {
    useEffect(() => {
        let totalCost = 0;
        values.tasks.forEach((task: Task, index: number) => {
            if (task.startDate && task.estimatedEndDate) {
                const periods: PeriodBudget[] = getPeriodsForms(task.startDate, task.estimatedEndDate, task.periodBudgets);
                const estimatedCost: number = periods.length > 0 ? periods.map((period: PeriodBudget): number => period.amount).reduce((accumulator = 0, currentValue: number) => accumulator + (currentValue ? currentValue : 0)) : 0;

                if (JSON.stringify(periods) !== JSON.stringify(task.periodBudgets)) {
                    setFieldValue(`tasks.${index}.periodBudgets`, periods);
                }

                if (estimatedCost !== task.estimatedCost) {
                    setFieldValue(`tasks.${index}.estimatedCost`, estimatedCost);
                }

                if (task.isPaidByFact && task.startDate != undefined) {
                    setFieldValue(`tasks.${index}.startDate`, undefined);
                }

                if (task.isPaidByFact && task.estimatedEndDate != undefined) {
                    setFieldValue(`tasks.${index}.estimatedEndDate`, undefined);
                }

                if (task.isPaidByFact && task.estimatedCost != undefined) {
                    setFieldValue(`tasks.${index}.estimatedCost`, undefined);
                }

                if (task.isPaidByFact && task.periodBudgets.length > 0) {
                    setFieldValue(`tasks.${index}.periodBudgets`, []);
                }

                totalCost = totalCost + estimatedCost;
            }
        });

        if (values.estimatedCost !== totalCost) {
            setFieldValue("estimatedCost", totalCost);
        }
    }, [ values.tasks ]);

    return (
        <tr>
            <td>
                <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!getIn(errors, "tasks." + index + ".title")} name={`tasks.${index}.title`} theme="no-margins" type="text" validated={submitCount > 0} value={values.tasks[index].title} onChange={setFieldValue} />
            </td>
            <td>
                <FormSelect invalid={!!getIn(errors, "tasks." + index + ".isPaidByFact")} name={`tasks.${index}.isPaidByFact`} options={getYesNoOptions()} theme="no-margins" type="boolean" validated={submitCount > 0} value={values.tasks[index].isPaidByFact ? "true" : "false"} onChange={setFieldValue} />
            </td>
            <td>
                {!values.tasks[index].isPaidByFact && <FormDatePicker icon={<Icon iconClass="calendar-week" type="fa" />} invalid={!!getIn(errors, "tasks." + index + ".startDate")} name={`tasks.${index}.startDate`} theme="no-margins" validated={submitCount > 0} value={values.tasks[index].startDate} onChange={setFieldValue} />}
            </td>
            <td>
                {!values.tasks[index].isPaidByFact && <FormDatePicker icon={<Icon iconClass="calendar-week" type="fa" />} invalid={!!getIn(errors, "tasks." + index + ".estimatedEndDate")} name={`tasks.${index}.estimatedEndDate`} theme="no-margins" validated={submitCount > 0} value={values.tasks[index].estimatedEndDate} onChange={setFieldValue} />}
            </td>
            <td>
                {!values.tasks[index].isPaidByFact && <FormInput disabled={true} icon={<Icon iconClass="dollar-sign" type="fa" />} invalid={!!getIn(errors, "tasks." + index + ".estimatedCost")} name={`tasks.${index}.estimatedCost`} theme="no-margins" type="number" validated={submitCount > 0} value={values.tasks[index].estimatedCost} onChange={setFieldValue} />}
            </td>
            <td>
                {!values.tasks[index].isPaidByFact && values.tasks[index].periodBudgets.map((budget: PeriodBudget, i: number) =>
                    <FormInput icon={<Icon iconClass="dollar-sign" type="fa" />} invalid={!!getIn(errors, "tasks." + index + ".periodBudgets." + i + ".amount")} key={`values.tasks[${index}].periodBudgets[${i}].year`} label={formattedPeriod(values.tasks[index].periodBudgets[i])} name={`tasks.${index}.periodBudgets.${i}.amount`} required={true} type="number" validated={submitCount > 0} value={values.tasks[index].periodBudgets[i].amount} onChange={setFieldValue} />
                )}
            </td>
        </tr>
    );
};

export default TaskFields;
