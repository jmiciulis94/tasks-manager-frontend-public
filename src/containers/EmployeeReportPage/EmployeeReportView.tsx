import React from "react";

import EmployeeReportTable from "../../components/EmployeeReportTable";
import { Employee, fullName } from "../../domain/employee";
import { EmployeeReport } from "../../domain/report";

export type EmployeeReportTableProps = {
    employee: Employee;
    employeeReport?: EmployeeReport;
};

const EmployeeReportView = ({ employeeReport, employee }: EmployeeReportTableProps) : JSX.Element => {

    if (employeeReport && employeeReport.entries.length !== 0) {
        return (
            <>
                <EmployeeReportTable entries={employeeReport.entries} projects={employeeReport.projects} tasks={employeeReport.tasks} totals={employeeReport.totals} />
            </>
        );
    } else {
        return (
            <>No time cards created for employee: {fullName(employee.firstName, employee.lastName)}.</>
        );
    }
};

export default EmployeeReportView;
