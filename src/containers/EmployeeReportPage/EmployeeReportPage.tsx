import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, Page } from "smart-tech-solutions-frontend-components";

import * as apiActions from "../../actions/apiActions";
import * as employeeActions from "../../actions/employeeActions";
import * as reportActions from "../../actions/reportActions";
import SelectEmployee from "../../components/SelectEmployee";
import { RootState } from "../../reducers/initialState";

import EmployeeReportView from "./EmployeeReportView";

const mapStateToProps = (state: RootState) => {
    return {
        employee: state.employee.employee,
        employeeList: state.employee.employeeList,
        employeeReport: state.report.employeeReport,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages
    };
};

const mapDispatchToProps = {
    getEmployee: employeeActions.getEmployee,
    getEmployeeList: employeeActions.getEmployeeList,
    getEmployeeReport: reportActions.getEmployeeReport,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const EmployeeReportPage = (props: PropsFromRedux) => {

    const [ selectedEmployeeId, setSelectedEmployeeId ] = useState<number | null>(null);

    useEffect(() => {
        props.getEmployeeList();
        setSelectedEmployeeId(null);
    }, []);

    const handleEmployeeChange = (employeeId: string) => {
        if (employeeId) {
            setSelectedEmployeeId(parseInt(employeeId));
            props.getEmployee(parseInt(employeeId));
            props.getEmployeeReport(parseInt(employeeId));
        }
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Employee report", url: "/employee-report" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Employee report">
            <SelectEmployee employeeList={props.employeeList} selectedEmployeeId={selectedEmployeeId ? selectedEmployeeId.toString() : null} onEmployeeChange={handleEmployeeChange} />
            {props.employee && selectedEmployeeId ? <EmployeeReportView employee={props.employee} employeeReport={props.employeeReport} /> : <>Please select an employee to display his/her report table.</>}
        </Page>
    );
};

export default connector(EmployeeReportPage);
