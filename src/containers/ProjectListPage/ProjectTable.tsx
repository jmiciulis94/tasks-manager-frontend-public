import React from "react";
import { Link } from "react-router-dom";
import { Table, TableActions } from "smart-tech-solutions-frontend-components";

import { Project, Task } from "../../domain/project";

export type ProjectTableProps = {
    onDelete: (id: number) => void;
    projectList: Project[];
};

const ProjectTable = ({ onDelete, projectList }: ProjectTableProps): JSX.Element => (
    <Table headers={[ "No", "Title", "Estimated Cost", "Start Date", "Estimated End Date", "Tasks", "Actions" ]}>
        {projectList.map((project: Project, i: number) => (
            <tr key={project.id}>
                <td className="sts-table-numeric-column">{i + 1}</td>
                <td>{project.title}</td>
                <td>{project.estimatedCost}</td>
                <td>{project.startDate}</td>
                <td>{project.estimatedEndDate}</td>
                <td>
                    {project.tasks.map((task: Task, i: number) => (
                        <React.Fragment key={task.id}>
                            {i !== 0 && <br />}
                            <Link to={"/project/" + project.id + "/task/" + task.id}>{task.title}</Link>
                        </React.Fragment>
                    ))}
                </td>
                <TableActions editUrl={"/project/" + project.id + "?mode=edit"} id={project.id || 0} viewUrl={"/project/" + project.id + "?mode=view"} onDelete={onDelete} />
            </tr>
        ))}
    </Table>
);

export default ProjectTable;
