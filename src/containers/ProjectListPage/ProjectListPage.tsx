import React, { useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, Page, showDeleteAlert } from "smart-tech-solutions-frontend-components";
import { SweetAlertResult } from "sweetalert2";

import * as apiActions from "../../actions/apiActions";
import * as projectActions from "../../actions/projectActions";
import { RootState } from "../../reducers/initialState";

import ProjectTable from "./ProjectTable";

const mapStateToProps = (state: RootState) => {
    return {
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        projectList: state.project.projectList,
        successMessages: state.api.successMessages
    };
};

const mapDispatchToProps = {
    deleteProject: projectActions.deleteProject,
    getProjectList: projectActions.getProjectList,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const ProjectListPage = (props: PropsFromRedux): JSX.Element => {
    useEffect(() => {
        props.getProjectList();
    }, []);

    const handleDelete = async (id: number): Promise<void> => {
        const result: SweetAlertResult = await showDeleteAlert();
        if (result.value) {
            await props.deleteProject(id);
            window.scrollTo(0, 0);
        }
    };

    const renderNoProjectsMessage = (): JSX.Element => (
        <>You don&#39;t have any projects yet.</>
    );

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Projects", url: "/project-list" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Projects">
            {props.projectList.length > 0 ? <ProjectTable projectList={props.projectList} onDelete={handleDelete} /> : renderNoProjectsMessage()}
        </Page>
    );
};

export default connector(ProjectListPage);
