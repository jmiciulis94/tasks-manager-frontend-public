import React from "react";
import { Icon, Level1MenuItem, MenuGroupLabel, MenuSeparator, SideBarMenuWrapper } from "smart-tech-solutions-frontend-components";

const AppMenu = (): JSX.Element => (
    <SideBarMenuWrapper>
        <MenuSeparator />
        <MenuGroupLabel text="MAIN" />
        <Level1MenuItem icon={<Icon iconClass="gauge" type="mdi" />} text="Dashboard" url="/dashboard" />
        <MenuSeparator />
        <MenuGroupLabel text="DATA" />
        <Level1MenuItem icon={<Icon iconClass="calendar-multiple-check" type="mdi" />} text="Time-Cards" url="/timecard-list" />
        <Level1MenuItem icon={<Icon iconClass="human-male-female" type="mdi" />} text="Employees" url="/employee-list" />
        <Level1MenuItem icon={<Icon iconClass="briefcase" type="mdi" />} text="Projects" url="/project-list" />
        <MenuSeparator />
        <MenuGroupLabel text="REPORTS" />
        <Level1MenuItem icon={<Icon iconClass="calculator" type="mdi" />} text="By Task" url="/task-report" />
        <Level1MenuItem icon={<Icon iconClass="clipboard-account" type="mdi" />} text="By Employee" url="/employee-report" />
        <Level1MenuItem icon={<Icon iconClass="calendar-clock" type="mdi" />} text="By Month" url="/monthly-report" />
    </SideBarMenuWrapper>
);

export default AppMenu;
