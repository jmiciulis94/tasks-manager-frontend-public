import React from "react";

import { formattedPayRate, formattedWithCurrency, formattedWithMinutes, Timesheet } from "../../domain/report";

export type RenderTimesheetsProps = {
    index: number;
    timesheet: Timesheet;
};

const RenderTimesheets = ({ index, timesheet } : RenderTimesheetsProps) : JSX.Element => {
    if (timesheet) {
        return (
            <td key={timesheet.id}>{formattedWithMinutes(timesheet.minutes)} <br/> {formattedWithCurrency(timesheet.amount)} <br/> {formattedPayRate(timesheet, timesheet.typeOfPay)}</td>
        );
    } else {
        return (
            <td key={index}/>
        );
    }
};

export default RenderTimesheets;
