import React from "react";

import { fullName } from "../../domain/employee";
import {
    EmployeeTimesheet,
    formattedWithCurrency,
    formattedWithMinutes,
    TaskHeader,
    Totals
} from "../../domain/report";
import RenderTimesheets from "../RenderTimesheets";

export type MonthlyReportTableProps = {
    entries: EmployeeTimesheet[];
    tasks: TaskHeader[];
    totals: Totals;
};

const MonthlyReportTable = ({ entries, tasks, totals }: MonthlyReportTableProps): JSX.Element => {
    return (
        <table className="table table-bordered m-t-30 table-hover contact-list" data-paging="true" data-paging-size="10">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    {tasks.map((task: TaskHeader) =>
                        <th key={task.taskId}>{task.title}</th>
                    )}
                    <th>Total:</th>
                </tr>
            </thead>
            <tbody>
                {entries.map((entry: EmployeeTimesheet, index: number) =>
                    <tr key={index}>
                        <td>{fullName(entry.employee.firstName, entry.employee.lastName)}</td>
                        {entry.timesheets.map((timesheet, index) =>
                            <RenderTimesheets index={index} key={index} timesheet={timesheet} />
                        )}
                    </tr>
                )}
            </tbody>
            <tfoot>
                <tr>
                    <td>Total:</td>
                    {tasks.map((task: TaskHeader) =>
                        <td key={task.taskId}>{formattedWithMinutes(task.totalMinutes)} <br /> {formattedWithCurrency(task.totalAmount)}</td>
                    )}
                    <td> Monthly total: <br /> {formattedWithMinutes(totals.minutes)} <br /> {formattedWithCurrency(totals.amount)}</td>
                </tr>
            </tfoot>
        </table>
    );
};

export default MonthlyReportTable;
