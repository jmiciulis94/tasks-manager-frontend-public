import React from "react";

import { formattedDate, formattedWithCurrency, formattedWithMinutes, PeriodTimesheet, ProjectHeader, TaskHeader, Totals } from "../../domain/report";
import RenderTimesheets from "../RenderTimesheets";

export type EmployeeReportTableProps = {
    entries: PeriodTimesheet[];
    projects: ProjectHeader[];
    tasks: TaskHeader[];
    totals: Totals;
};

const EmployeeReportTable = ({ entries, projects, tasks, totals } : EmployeeReportTableProps) : JSX.Element => {
    return (
        <table className="table table-bordered m-t-30 table-hover contact-list" data-paging="true" data-paging-size="10">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    {projects.map((project: ProjectHeader) =>
                        <th colSpan={project.numberOfTasks} key={project.projectId}>{project.title}</th>)}
                    <th>&nbsp;</th>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    {tasks.map((task: TaskHeader) =>
                        <th key={task.taskId}>{task.title}</th>
                    )}
                    <th>Total:</th>
                </tr>
            </thead>
            <tbody>
                {entries.map((entry: PeriodTimesheet, index: number) =>
                    <tr key={index}>
                        <td>{formattedDate(entry.period.year, entry.period.month)}</td>
                        {entry.timesheets.map((timesheet, index) =>
                            <RenderTimesheets index={index} key={index} timesheet={timesheet} />
                        )}
                    </tr>
                )}
            </tbody>
            <tfoot>
                <tr>
                    <td>Total:</td>
                    {tasks.map((task: TaskHeader) =>
                        <td key={task.taskId}>{formattedWithMinutes(task.totalMinutes)} <br/> {formattedWithCurrency(task.totalAmount)}</td>
                    )}
                    <td> Employee total: <br/> {formattedWithMinutes(totals.minutes)} <br/> {formattedWithCurrency(totals.amount)}</td>
                </tr>
            </tfoot>
        </table>
    );
};

export default EmployeeReportTable;
