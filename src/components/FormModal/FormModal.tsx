import React, { ReactNode } from "react";

export type FormModalProps = {
    children: ReactNode;
    id: string;
    isSubmitting: boolean;
    title: string;
};

const FormModal = ({ children, id, isSubmitting, title }: FormModalProps): JSX.Element => (
    <div aria-hidden="true" aria-labelledby={`modal-${id}`} className="modal fade in" id={id} role="dialog">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <h4 className="modal-title" id={`modal-${id}`}>{title}</h4>
                    <button aria-hidden="true" className="close" data-dismiss="modal" type="button">×</button>
                </div>
                <div className="modal-body">
                    {children}
                </div>
                <div className="modal-footer">
                    <button className="btn hidden-sm-down btn-success mr-2" disabled={isSubmitting} type="submit">{<i className="mr-1 fa fa-check" />} {isSubmitting ? "Saving..." : "Save"}</button>
                    <button className="btn hidden-sm-down btn-inverse mr-2" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </div>
        </div>
    </div>
);

export default FormModal;
