import React from "react";

import { EmployeeHeader, formattedDate, formattedWithCurrency, formattedWithMinutes, PeriodTimesheet, Totals } from "../../domain/report";
import RenderTimesheets from "../RenderTimesheets";

export type TaskReportTableProps = {
    employees: EmployeeHeader[];
    entries: PeriodTimesheet[];
    totals: Totals;
};

const EmployeeReportTable = ({ employees, entries, totals } : TaskReportTableProps) : JSX.Element => {
    return (
        <table className="table table-bordered m-t-30 table-hover contact-list" data-paging="true" data-paging-size="10">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    {employees.map((employee: EmployeeHeader) =>
                        <th key={employee.employeeId}>{employee.fullName}</th>
                    )}
                    <th>Total:</th>
                </tr>
            </thead>
            <tbody>
                {entries.map((entry: PeriodTimesheet, index: number) =>
                    <tr key={index}>
                        <td>{formattedDate(entry.period.year, entry.period.month)}</td>
                        {entry.timesheets.map((timesheet, index) =>
                            <RenderTimesheets index={index} key={index} timesheet={timesheet} />
                        )}
                    </tr>
                )}
            </tbody>
            <tfoot>
                <tr>
                    <td>Total:</td>
                    {employees.map((employee: EmployeeHeader) =>
                        <td key={employee.employeeId}>{formattedWithMinutes(employee.totalMinutes)} <br/> {formattedWithCurrency(employee.totalAmount)}</td>
                    )}
                    <td> Task total: <br/> {formattedWithMinutes(totals.minutes)} <br/> {formattedWithCurrency(totals.amount)}</td>
                </tr>
            </tfoot>
        </table>
    );
};

export default EmployeeReportTable;
