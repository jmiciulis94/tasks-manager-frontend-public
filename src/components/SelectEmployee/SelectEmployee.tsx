import React from "react";
import { FormSelectStateless } from "smart-tech-solutions-frontend-components";

import { Employee, getEmployeeOptions } from "../../domain/employee";

type SelectEmployeeType = {
    employeeList: Employee[];
    onEmployeeChange: (employeeId: string) => void;
    selectedEmployeeId: string | null;
};

const SelectEmployee = ({ employeeList, onEmployeeChange, selectedEmployeeId }: SelectEmployeeType): JSX.Element => (
    <FormSelectStateless label="Employee:" options={getEmployeeOptions(employeeList)} value={selectedEmployeeId || ""} onChange={onEmployeeChange} />
);

export default SelectEmployee;
